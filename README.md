# Ocelot Desktop
![Banner][banner]

A desktop version of the renowned OpenComputers emulator Ocelot.

[**Download** the latest build][download]

## Why
You might already be happy with your choice of an OC emulator; after all, there
is already a plenty of them.
So why would you want to reconsider your life choices now all of a sudden?
A fine question, indeed; perhaps, a list of features will persuade you.

### Powered by ocelot-brain
At the heart of this emulator is [ocelot-brain][ocelot-brain] (uh, don't ask me),
which is essentially the source code of OpenComputers stripped of everything
Minecraft-specific and packaged as a Scala library.
This makes Ocelot Desktop **the most accurate emulator** ever made.
Your programs will run on the same version of Lua as used by the mod and have
similar timings.
The performance and memory constraints present in OC are also emulated.

### Customizable setups
Computers can have the following components:

- the 3 tiers of graphics cards
- all kinds of network cards (wired, wireless)
- a linked card
- an internet card
- a redstone card in the both tiers
- a data card (again, you can pick any of the three tiers)
- hard disks
- a floppy disk (in T3+ computer cases only)

The choice is restricted by the tier of a computer case, just like in
OpenComputers, to avoid building impossible configurations.
Oh, did I forget to mention that Ocelot Desktop has both the CPUs and the APUs?
Memory can likewise be installed according to your needs.

If one computer is not enough, you can add another one.
Or two, or a thousand, as long as your host doesn't collapse under the load,
of course.
The network cards are there for a reason — these newly spawned machines can
communicate with each other.
And relays may help you manage the wired networks.

Or, instead of employing an army of computers, you might want to connect a dozen
of screens to a single machine, like in the movies.
No problem — we've got that covered, too.

### Pretty graphical interface
![GUI][gui]

A slick interface allows you to customize the setup to your liking.
Add more computers, organize the connections between components, build complex
setups, and manage your screen real estate to avoid distractions.

Many additional options are hidden in the context menu — try hitting the
right mouse button on the various things.
For example, components will let you copy their address to the clipboard, and
right-clicking on the TPS counter on the bottom allows you to change the
simulation speed.

The emulator uses hardware acceleration to offload the daunting task of
rendering its interface to a specialized device, so make sure you have a OpenGL
2.1-capable graphics card.

### Persistable workspaces
It would be sad if, after all the hard work you put into adjusting the
workspace, you have to do that again.
I mean, OpenComputers can persist its machines just fine, right?
By basing the emulator on its code, we've essentially inherited the ability
to save workspaces on the disk and load them afterwards.

Just in case, Ocelot Desktop will warn you if you smash the quit button without
saving.

### Cool features
![Performance graphs][graphs]

A few smaller features are worth mentioning, too:

- Screens are resizeable — drag the bottom-right corner.
- Windows are labeled with the corresponding block's address; however, you can
  set a custom label — look for an option in the context menu.
- The button in a computer case window shows the performance graphs:
  the used memory, processor time and call budget.
- Hold the Ctrl key while dragging blocks to have them snap to the grid.

## Download
Decided to give Ocelot Desktop a shot? [**Download** the latest build][download].

## How to build it?
Just import the project in your favorite IDE.  
Make sure to have Scala and SBT installed (manually or through IDE).

Use `sbt run` to start Ocelot Desktop. Use `sbt assembly` to generate JAR file.  
(It will appear at `target/scala-2.13/ocelot-desktop.jar` location.)

## Credits
- **LeshaInc**, the author and maintainer of Ocelot Desktop.
- **Totoro**, the creator of ocelot-brain and ocelot-online.
- **bpm140**, who created marvelous Ocelot Desktop landing page.
- **rason**, who stirred the development at the critical moment.
- **fingercomp**, who wrote this README.

## See also
- [Ocelot Desktop][ocelot-desktop] web page (link to the latest build, FAQ)
- [ocelot.online][ocelot-online], a rudimentary web version of Ocelot
- [ocelot-brain][ocelot-brain], the backend library of Ocelot Desktop
- [#cc.ru on IRC][irc] if you have any questions
  (Russian, but we're fine with English too)
- [Discord][discord] if you prefer so (we will not judge)

[banner]: https://i.imgur.com/OzkpQZv.png
[download]: https://cc-ru.gitlab.io/ocelot/ocelot-desktop/ocelot.jar
[gui]: https://i.imgur.com/O4bF7I8.png
[graphs]: https://i.imgur.com/mG8UjhV.png
[ocelot-brain]: https://gitlab.com/cc-ru/ocelot/ocelot-brain
[ocelot-online]: https://ocelot.fomalhaut.me/
[ocelot-desktop]: https://ocelot.fomalhaut.me/desktop/
[irc]: ircs://irc.esper.net:6697/cc.ru
[discord]: https://discord.com/invite/FM9qWGm
