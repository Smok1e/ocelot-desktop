name := "ocelot-desktop"
version := "1.1.0"
scalaVersion := "2.13.8"

lazy val root = project.in(file("."))
  .dependsOn(brain % "compile->compile")
  .aggregate(brain)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](
      BuildInfoKey.action("commit") {
        scala.sys.process.Process("git rev-parse HEAD").!!.trim
      }, version
    )
  )

lazy val brain = ProjectRef(file("lib/ocelot-brain"), "ocelot-brain")

libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.18.0"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.18.0"
libraryDependencies += "li.flor" % "native-j-file-chooser" % "1.6.4"

val lwjglVersion = "2.9.3"

libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl" % lwjglVersion
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-linux"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-windows"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-osx"

libraryDependencies += "org.jcraft" % "jorbis" % "0.0.17"

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}

assembly / assemblyJarName := s"ocelot-desktop.jar"
