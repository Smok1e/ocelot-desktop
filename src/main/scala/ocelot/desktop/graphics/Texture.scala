package ocelot.desktop.graphics

import ocelot.desktop.util.{Logging, Resource, ResourceManager}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl._

import java.awt.image.BufferedImage
import java.nio.ByteBuffer

class Texture extends Logging with Resource {
  val texture: Int = GL11.glGenTextures()

  ResourceManager.registerResource(this)

  def freeResource(): Unit = {
    logger.debug(s"Destroyed texture (ID: $texture)")
    GL11.glDeleteTextures(texture)
  }

  bind()
  GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR)
  GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST)
  GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP)
  GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP)

  def this(image: BufferedImage) {
    this()

    val pixels = new Array[Int](image.getWidth * image.getHeight)
    image.getRGB(0, 0, image.getWidth, image.getHeight, pixels, 0, image.getWidth)

    val buf = BufferUtils.createByteBuffer(image.getWidth * image.getHeight * 4)

    for (y <- 0 until image.getHeight) {
      for (x <- 0 until image.getWidth) {
        val pixel = pixels(y * image.getWidth + x)
        buf.put(((pixel >> 16) & 0xFF).toByte)
        buf.put(((pixel >> 8) & 0xFF).toByte)
        buf.put((pixel & 0xFF).toByte)
        buf.put(((pixel >> 24) & 0xFF).toByte)
      }
    }

    buf.flip()

    bind()
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, image.getWidth, image.getHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf)
    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
  }

  def this(width: Int, height: Int, format: Int, dataType: Int) = {
    this()
    bind()
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, format, width, height, 0, format, dataType, null.asInstanceOf[ByteBuffer])
  }

  def set(width: Int, height: Int, format: Int, dataType: Int, buf: ByteBuffer): Unit = {
    bind()
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, format, width, height, 0, format, dataType, buf)
    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
  }

  def this(width: Int, height: Int, format: Int, dataType: Int, buf: ByteBuffer) = {
    this()
    set(width, height, format, dataType, buf)
  }

  def write(x: Int, y: Int, w: Int, h: Int, format: Int, dataType: Int, buf: ByteBuffer): Unit = {
    bind()
    GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, 0, x, y, w, h, format, dataType, buf)
    GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
  }

  def bind(unit: Int = 0): Unit = {
    GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit)
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture)
  }
}
