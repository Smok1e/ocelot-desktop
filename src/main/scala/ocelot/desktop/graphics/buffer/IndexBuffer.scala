package ocelot.desktop.graphics.buffer

import org.lwjgl.opengl.GL15

class IndexBuffer extends Buffer[Index] {
  override val target: Int = GL15.GL_ELEMENT_ARRAY_BUFFER

  def this(elements: Seq[Index]) = {
    this()
    createStatic(elements)
  }
}
