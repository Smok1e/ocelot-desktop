package ocelot.desktop.graphics.mesh

import ocelot.desktop.geometry.Vector2D
import ocelot.desktop.graphics.buffer.Index

object Mesh {
  val quad: Mesh = Mesh(
    Array(Vector2D(0f, 0f), Vector2D(1f, 0f), Vector2D(1f, 1f),
      Vector2D(1f, 1f), Vector2D(0f, 1f), Vector2D(0f, 0f)).map(v => MeshVertex(v, v)),
  )
}

case class Mesh(vertices: Seq[MeshVertex], indices: Option[Seq[Index]] = None)
