package ocelot.desktop.graphics.mesh

import ocelot.desktop.geometry.Vector2D
import org.lwjgl.opengl.GL11

import java.nio.ByteBuffer

object MeshVertex extends VertexType {
  override val stride: Int = 16

  override val attributes: Seq[Attribute] = Array(
    Attribute("inPos", 2, GL11.GL_FLOAT, normalized = false, stride, 0),
    Attribute("inUV", 2, GL11.GL_FLOAT, normalized = false, stride, 8)
  )
}

case class MeshVertex(pos: Vector2D, uv: Vector2D) extends Vertex {
  override def stride: Int = MeshVertex.stride

  override def vertexType: VertexType = MeshVertex

  override def put(buf: ByteBuffer): Unit = {
    buf.putFloat(pos.x)
    buf.putFloat(pos.y)
    buf.putFloat(uv.x)
    buf.putFloat(uv.y)
  }
}
