package ocelot.desktop.graphics.mesh

trait VertexType {
  val stride: Int
  val attributes: Seq[Attribute]
}

case class Attribute(name: String, size: Int, ty: Int, normalized: Boolean, stride: Int, pointer: Long)