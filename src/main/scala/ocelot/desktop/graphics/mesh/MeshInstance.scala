package ocelot.desktop.graphics.mesh

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Transform2D
import org.lwjgl.opengl.GL11

import java.nio.ByteBuffer

object MeshInstance extends VertexType {
  override val stride: Int = 104

  override val attributes: Seq[Attribute] = Array(
    Attribute("inColor", 4, GL11.GL_FLOAT, normalized = false, stride, 0),
    Attribute("inTextColor", 4, GL11.GL_FLOAT, normalized = false, stride, 16),
    Attribute("inTransform0", 3, GL11.GL_FLOAT, normalized = false, stride, 32),
    Attribute("inTransform1", 3, GL11.GL_FLOAT, normalized = false, stride, 44),
    Attribute("inUVTransform0", 3, GL11.GL_FLOAT, normalized = false, stride, 56),
    Attribute("inUVTransform1", 3, GL11.GL_FLOAT, normalized = false, stride, 68),
    Attribute("inTextUVTransform0", 3, GL11.GL_FLOAT, normalized = false, stride, 80),
    Attribute("inTextUVTransform1", 3, GL11.GL_FLOAT, normalized = false, stride, 92),
  )
}

case class MeshInstance(color: Color, textColor: Color, transform: Transform2D, uvTransform: Transform2D,
                        textUvTransform: Transform2D) extends Vertex
{
  override def stride: Int = MeshInstance.stride

  override def vertexType: VertexType = MeshInstance

  override def put(buf: ByteBuffer): Unit = {
    color.toRGBANorm.components.foreach(f => buf.putFloat(f))
    textColor.toRGBANorm.components.foreach(f => buf.putFloat(f))
    transform.array.foreach(f => buf.putFloat(f))
    uvTransform.array.foreach(f => buf.putFloat(f))
    textUvTransform.array.foreach(f => buf.putFloat(f))
  }
}
