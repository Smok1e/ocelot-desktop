package ocelot.desktop.graphics.mesh

import ocelot.desktop.graphics.buffer.BufferPut

trait Vertex extends BufferPut {
  def vertexType: VertexType
}
