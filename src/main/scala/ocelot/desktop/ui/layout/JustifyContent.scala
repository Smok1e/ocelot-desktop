package ocelot.desktop.ui.layout

object JustifyContent extends Enumeration {
  val Start, End, Center = Value
}
