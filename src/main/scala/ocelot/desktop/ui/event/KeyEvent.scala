package ocelot.desktop.ui.event

object KeyEvent {
  object State extends Enumeration {
    val Press, Release, Repeat = Value
  }
}

case class KeyEvent(state: KeyEvent.State.Value, code: Int, char: Char) extends Event
