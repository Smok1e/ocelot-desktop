package ocelot.desktop.ui.event

import ocelot.desktop.geometry.Vector2D

object DragEvent {

  object State extends Enumeration {
    val Start, Drag, Stop = Value
  }

  def apply(state: DragEvent.State.Value, button: MouseEvent.Button.Value, mousePos: Vector2D,
            start: Vector2D, delta: Vector2D): DragEvent =
  {
    val ev = DragEvent(state, button, mousePos)
    ev._start = start
    ev._delta = delta
    ev
  }
}

case class DragEvent(state: DragEvent.State.Value, button: MouseEvent.Button.Value, mousePos: Vector2D) extends Event {
  private var _delta: Vector2D = Vector2D(0, 0)
  private var _start: Vector2D = Vector2D(0, 0)

  def start: Vector2D = _start

  def delta: Vector2D = _delta
}
