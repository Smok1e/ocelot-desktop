package ocelot.desktop.ui.event

import ocelot.desktop.geometry.Vector2D

case class ClickEvent(button: MouseEvent.Button.Value, mousePos: Vector2D) extends Event
