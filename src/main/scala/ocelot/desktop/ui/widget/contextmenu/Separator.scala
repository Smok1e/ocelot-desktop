package ocelot.desktop.ui.widget.contextmenu

import ocelot.desktop.ColorScheme
import ocelot.desktop.geometry.{Rect2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.widget.Widget

class Separator extends Widget {
  override def minimumSize: Size2D = Size2D(50, 9)

  override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 9)

  override def draw(g: Graphics): Unit = {
    g.rect(Rect2D(position + Vector2D(0, 4), size.copy(height = 1)),
      ColorScheme("ContextMenuSeparator"))
  }
}
