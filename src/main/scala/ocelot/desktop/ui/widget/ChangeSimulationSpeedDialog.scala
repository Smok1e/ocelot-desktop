package ocelot.desktop.ui.widget

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.util.Orientation

class ChangeSimulationSpeedDialog() extends ModalDialog {
  private var tickInterval: Option[Long] = Some(OcelotDesktop.ticker.tickInterval)

  private def confirm(): Unit = {
    if (tickInterval.isDefined)
      OcelotDesktop.ticker.tickInterval = tickInterval.get
    close()
  }

  children :+= new PaddingBox(new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text = "Change simulation speed"
    }, Padding2D(bottom = 16))

    private var inputTPS: TextInput = _
    private var inputMSPT: TextInput = _
    private def formatMSPT(v: Long) = (v.toFloat / 1000).toString
    private def formatTPS(v: Long) = (1000000 / v.toFloat).toString

    inputMSPT = new TextInput(formatMSPT(OcelotDesktop.ticker.tickInterval)) {
      focus()

      override def onInput(text: String): Unit = {
        tickInterval = try {
          val v = (text.toFloat * 1000).toLong
          inputTPS.setInput(formatTPS(v))
          Some(v)
        } catch {
          case _: NumberFormatException => None
        }
      }

      override def isInputValid(text: String): Boolean = {
        try {
          text.toFloat
          true
        } catch {
          case _: NumberFormatException => false
        }
      }

      override def onConfirm(): Unit = confirm()
    }

    inputTPS = new TextInput(formatTPS(OcelotDesktop.ticker.tickInterval)) {
      override def onInput(text: String): Unit = {
        tickInterval = try {
          val v = ((1 / text.toFloat) * 1000000).toLong
          inputMSPT.setInput(formatMSPT(v))
          Some(v)
        } catch {
          case _: NumberFormatException => None
        }
      }

      override def isInputValid(text: String): Boolean = {
        try {
          text.toFloat
          true
        } catch {
          case _: NumberFormatException => false
        }
      }

      override def onConfirm(): Unit = confirm()
    }

    children :+= new Label {
      override def text = "Milliseconds per tick:"
    }
    children :+= new PaddingBox(inputMSPT, Padding2D(bottom = 8))
    children :+= new Label {
      override def text = "Ticks per second:"
    }
    children :+= new PaddingBox(inputTPS, Padding2D(bottom = 8))

    children :+= new Widget {
      children :+= new Filler

      children :+= new PaddingBox(new Button {
        override def text: String = "Cancel"
        override def onClick(): Unit = close()
      }, Padding2D(right = 8))

      children :+= new Button {
        override def text: String = "Apply"
        override def onClick(): Unit = confirm()
      }
    }
  }, Padding2D.equal(16))
}
