package ocelot.desktop.ui.widget.settings

import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.Orientation

trait SettingsTab extends Widget {
  override protected val layout = new LinearLayout(this, orientation = Orientation.Vertical)

  val icon: String
  val label: String

  /**
    * Callback that will be called by the parent Settings dialog when settings were changed
    * from outside of the tab (for example user pressed "Cancel" and they were reset).
    * Apply necessary immediate actions here.
    * (Like adjusting volume for sound resources in case of Sound settings.)
    */
  def applySettings(): Unit = {}
}
