package ocelot.desktop.ui.widget.settings

import ocelot.desktop.Settings
import ocelot.desktop.audio.SoundSource
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.ui.widget.{PaddingBox, Slider}
import ocelot.desktop.util.ResourceManager

class SoundSettingsTab extends SettingsTab {
  override val icon: String = "icons/SettingsSound"
  override val label: String = "Sound"

  override def applySettings(): Unit = {
    ResourceManager.forEach {
      case soundSource: SoundSource => soundSource.setVolume(Settings.get.volumeEnvironment * Settings.get.volumeMaster)
      case _ =>
    }
  }

  children :+= new PaddingBox(new Slider(Settings.get.volumeMaster, "Master Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeMaster = value
      applySettings()
    }
  }, Padding2D(bottom = 8))

  children :+= new PaddingBox(new Slider(Settings.get.volumeBeep, "Beep Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeBeep = value
    }
  }, Padding2D(bottom = 8))

  children :+= new PaddingBox(new Slider(Settings.get.volumeEnvironment, "Environment Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeEnvironment = value
      applySettings()
    }
  }, Padding2D(bottom = 8))
}
