package ocelot.desktop.ui.widget.settings

import ocelot.desktop.Settings
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.ui.widget.{Checkbox, PaddingBox}

class UISettingsTab extends SettingsTab {
  override val icon: String = "icons/SettingsUI"
  override val label: String = "UI"

  children :+= new PaddingBox(new Checkbox("Move workspace windows together with workspace",
      initialValue = Settings.get.stickyWindows) {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Boolean): Unit = {
      Settings.get.stickyWindows = value
    }
  }, Padding2D(bottom = 8))
}
