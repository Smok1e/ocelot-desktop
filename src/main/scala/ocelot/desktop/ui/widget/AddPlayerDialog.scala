package ocelot.desktop.ui.widget

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.util.Orientation

class AddPlayerDialog() extends ModalDialog {
  var name = ""

  private def confirm(): Unit = {
    OcelotDesktop.selectPlayer(name)
    close()
  }

  children :+= new PaddingBox(new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text = "Add new player"
    }, Padding2D(bottom = 16))

    children :+= new PaddingBox(new TextInput("") {
      focus()
      override def onInput(text: String): Unit = name = text
      override def onConfirm(): Unit = confirm()
    }, Padding2D(bottom = 8))

    children :+= new Widget {
      children :+= new Filler
      children :+= new Button {
        override def text: String = "Cancel"
        override def onClick(): Unit = close()
      }
      children :+= new PaddingBox(new Button {
        override def text: String = "Ok"
        override def onClick(): Unit = confirm()
      }, Padding2D(left = 8))
    }
  }, Padding2D.equal(16))
}
