package ocelot.desktop.ui.widget

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.{Graphics, IconDef}
import ocelot.desktop.util.Spritesheet

class Icon(icon: IconDef) extends Widget {
  def this(name: String) {
    this(IconDef(name))
  }

  def this(name: String, color: Color) {
    this(IconDef(name, color = color))
  }

  def this(name: String, animation: IconDef.Animation) {
    this(IconDef(name, animation = Some(animation)))
  }

  override def minimumSize: Size2D = {
    val size = Spritesheet.spriteSize(icon.icon) * icon.sizeMultiplier
    icon.animation match {
      case None => size
      case Some(_) => size.copy(height = size.width)
    }
  }

  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.sprite(icon.icon, bounds.x, bounds.y, bounds.w, bounds.h, icon.color, icon.animation)
  }
}