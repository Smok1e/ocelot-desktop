package ocelot.desktop.ui.widget.window

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{Label, PaddingBox, Widget}
import ocelot.desktop.util.{DrawUtils, Orientation, Profiler}

class ProfilerWindow extends BasicWindow {
  private val inner: Widget = new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)
  }

  children :+= new PaddingBox(inner, Padding2D.equal(8))

  override def draw(g: Graphics): Unit = {
    inner.children = Array()
    for (line <- Profiler.report()) {
      inner.children :+= new Label {
        override def text: String = line
      }
    }

    recalculateBounds()
    relayout()

    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
