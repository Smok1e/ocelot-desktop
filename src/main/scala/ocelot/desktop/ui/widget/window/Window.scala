package ocelot.desktop.ui.widget.window

import ocelot.desktop.geometry.{Rect2D, Vector2D}
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.event.handlers.DragHandler
import ocelot.desktop.ui.event.{DragEvent, KeyEvent, MouseEvent}
import ocelot.desktop.ui.widget.Widget
import org.lwjgl.input.Keyboard
import totoro.ocelot.brain.nbt.NBTTagCompound

trait Window extends Widget with DragHandler {
  protected var _isFocused: Boolean = false
  protected var _isOpen: Boolean = false

  private var grabPoint: Option[Vector2D] = None

  override def receiveMouseEvents = true

  eventHandlers += {
    case MouseEvent(MouseEvent.State.Press, _) =>
      windowPool.changeFocus(this)

    case ev@DragEvent(DragEvent.State.Start, MouseEvent.Button.Left, mousePos) =>
      if (dragRegions.exists(_.contains(ev.start))) {
        grabPoint = Some(mousePos - position)
      }

    case DragEvent(DragEvent.State.Drag, MouseEvent.Button.Left, mousePos) =>
      for (g <- grabPoint) {
        position = mousePos - g
      }

    case DragEvent(DragEvent.State.Stop, MouseEvent.Button.Left, _) =>
      grabPoint = None

    case KeyEvent(KeyEvent.State.Release, Keyboard.KEY_ESCAPE, _) if canCloseOnEsc =>
      hide()
  }

  private def canCloseOnEsc: Boolean = isFocused && !root.get.modalDialogPool.isVisible

  protected def windowPool: WindowPool = parent.get.asInstanceOf[WindowPool]

  protected def dragRegions: Iterator[Rect2D] = Iterator(Rect2D(position, size))

  def save(nbt: NBTTagCompound): Unit = {
    nbt.setBoolean("focused", _isFocused)
    nbt.setBoolean("open", _isOpen)
  }

  def load(nbt: NBTTagCompound): Unit = {
    _isFocused = nbt.getBoolean("focused")
    _isOpen = nbt.getBoolean("open")
    if (_isOpen) {
      UiHandler.root.workspaceView.windowPool.addWindow(this)
    }
  }

  def isFocused: Boolean = _isFocused

  def isOpen: Boolean = _isOpen

  def focus(): Unit = {
    _isFocused = true
  }

  def unfocus(): Unit = {
    _isFocused = false
  }

  def show(): Unit = {
    _isOpen = true
    _isFocused = true
  }

  def hide(): Unit = {
    _isOpen = false
    _isFocused = false
  }

  override def update(): Unit = {
    super.update()

    if (canCloseOnEsc)
      root.get.statusBar.addKeyEntry("ESC", "Close window")
  }
}
