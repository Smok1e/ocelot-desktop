package ocelot.desktop.ui.widget.window

import ocelot.desktop.geometry.Vector2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.util.animation.ValueAnimation
import totoro.ocelot.brain.nbt.NBTTagCompound

trait BasicWindow extends Window {
  private val alpha = new ValueAnimation(speed = 7)

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    alpha.save(nbt, "alpha")
    position.save(nbt)
  }

  override def load(nbt: NBTTagCompound): Unit = {
    alpha.load(nbt, "alpha")
    position = new Vector2D(nbt)
    super.load(nbt)
  }

  override def show(): Unit = {
    super.show()
    alpha.goto(1f)
  }

  override def hide(): Unit = {
    super.hide()
    alpha.goto(0f)
  }

  override def focus(): Unit = {
    _isFocused = true
    alpha.goto(1f)
  }

  override def unfocus(): Unit = {
    _isFocused = false
    alpha.goto(0.5f)
  }

  override def update(): Unit = {
    super.update()
    alpha.update()

    if (alpha.value < 0.001) {
      windowPool.closeWindow(this)
    }
  }

  def beginDraw(g: Graphics): Unit = {
    g.save()
    if (alpha.value < 1f)
      g.beginGroupAlpha()
  }

  def endDraw(g: Graphics): Unit = {
    if (alpha.value < 1f)
      g.endGroupAlpha(alpha.value)
    g.restore()
  }

  override def drawChildren(g: Graphics): Unit = {
    super.drawChildren(g)
  }
}
