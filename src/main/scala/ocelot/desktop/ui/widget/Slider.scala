package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, DragHandler}
import ocelot.desktop.ui.event.{ClickEvent, DragEvent, MouseEvent}
import ocelot.desktop.util.DrawUtils
import ocelot.desktop.util.MathUtils.ExtendedFloat

class Slider(var value: Float, text: String) extends Widget with ClickHandler with DragHandler {
  def onValueChanged(value: Float): Unit = {}

  override def receiveMouseEvents: Boolean = true

  private val handleWidth = 8.0f

  private def calculateValue(x: Float): Unit = {
    value = ((x - bounds.x - handleWidth / 2f) / (bounds.w - handleWidth)).clamp(0f, 1f)
    onValueChanged(value)
  }

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, pos) => calculateValue(pos.x)
    case DragEvent(_, MouseEvent.Button.Left, pos) => calculateValue(pos.x)
  }

  override def minimumSize: Size2D = Size2D(24 + text.length * 8, 24)
  override def maximumSize: Size2D = minimumSize.copy(width = Float.PositiveInfinity)

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("SliderBackground"))
    DrawUtils.ring(g, position.x, position.y, width, height, 2, ColorScheme("SliderBorder"))

    g.rect(position.x + value * (bounds.w - handleWidth), position.y, handleWidth, height, ColorScheme("SliderHandler"))
    DrawUtils.ring(g, position.x + value * (bounds.w - handleWidth), position.y, handleWidth, height, 2, ColorScheme("SliderBorder"))

    g.background = Color.Transparent
    g.foreground = ColorScheme("SliderForeground")
    val fullText = f"$text: ${value * 100}%.0f%%"
    val textWidth = fullText.iterator.map(g.font.charWidth(_)).sum
    g.text(position.x + ((width - textWidth) / 2).round, position.y + 4, fullText)
  }
}
