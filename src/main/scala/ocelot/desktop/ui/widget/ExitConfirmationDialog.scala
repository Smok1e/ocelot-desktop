package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.util.Orientation

class ExitConfirmationDialog extends ModalDialog {
  children :+= new PaddingBox(new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text = "Save workspace before exiting?"
    }, Padding2D(bottom = 16))

    children :+= new Widget {
      children :+= new PaddingBox(new Button {
        override def text: String = "Cancel"
        override def onClick(): Unit = close()
      }, Padding2D(left = 8))

      children :+= new Filler

      children :+= new PaddingBox(new Button {
        override def text: String = "No"
        override def onClick(): Unit = onExitSelected()
      }, Padding2D(right = 8))

      children :+= new PaddingBox(new Button {
        override def text: String = "Yes"
        override def onClick(): Unit = onSaveSelected()
      }, Padding2D(right = 8))
    }
  }, Padding2D.equal(16))

  def onSaveSelected(): Unit = {}

  def onExitSelected(): Unit = {}
}
