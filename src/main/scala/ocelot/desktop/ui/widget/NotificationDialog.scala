package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.util.Orientation

class NotificationDialog(message: String) extends ModalDialog {
  children :+= new PaddingBox(new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :++= message.split('\n').map(line => new PaddingBox(new Label {
      override def text: String = line
    }, Padding2D(bottom = 8)))

    children :+= new Widget {
      children :+= new Filler

      children :+= new PaddingBox(new Button {
        override def text: String = "Ok"
        override def onClick(): Unit = close()
      }, Padding2D(right = 8, top = 8))
    }
  }, Padding2D.equal(16))

  def onSaveSelected(): Unit = {}

  def onExitSelected(): Unit = {}
}
