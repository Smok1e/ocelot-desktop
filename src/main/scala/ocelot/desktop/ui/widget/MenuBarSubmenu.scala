package ocelot.desktop.ui.widget

import ocelot.desktop.geometry.Vector2D
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.contextmenu.ContextMenu

class MenuBarSubmenu(label: String, setup: ContextMenu => Unit) extends MenuBarButton(label) {
  private var menuOpened = false

  override def onMouseLeave(): Unit = {
    if (!menuOpened) super.onMouseLeave()
  }

  override def onClick(): Unit = {
    menuOpened = true

    val btn = this
    val menu = new ContextMenu {
      override def close(): Unit = {
        super.close()
        btn.menuOpened = false
        btn.onMouseLeave()
      }
    }

    setup(menu)
    UiHandler.root.contextMenus.open(menu, position + Vector2D(0, height))
  }
}
