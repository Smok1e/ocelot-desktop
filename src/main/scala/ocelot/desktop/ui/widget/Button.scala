package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.ClickHandler
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import ocelot.desktop.util.DrawUtils

class Button extends Widget with ClickHandler {
  def text: String = ""
  def onClick(): Unit = {}

  override def receiveMouseEvents: Boolean = true

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) => onClick()
  }

  override def minimumSize: Size2D = Size2D(24 + text.length * 8, 24)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("ButtonBackground"))
    DrawUtils.ring(g, position.x, position.y, width, height, 2, ColorScheme("ButtonBorder"))

    g.background = Color.Transparent
    g.foreground = ColorScheme("ButtonForeground")
    val textWidth = text.iterator.map(g.font.charWidth(_)).sum
    g.text(position.x + ((width - textWidth) / 2).round, position.y + 4, text)
  }
}
