package ocelot.desktop.ui.widget
import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.event.handlers.ClickHandler
import ocelot.desktop.ui.event.sources.KeyEvents
import ocelot.desktop.ui.event.{ClickEvent, KeyEvent, MouseEvent}
import ocelot.desktop.util.DrawUtils
import ocelot.desktop.util.animation.ColorAnimation
import org.lwjgl.input.Keyboard

import scala.collection.mutable.ArrayBuffer

class TextInput(val initialText: String = "") extends Widget with ClickHandler {
  def onInput(text: String): Unit = {}
  def onConfirm(): Unit = {}
  def isInputValid(text: String): Boolean = true

  var isFocused = false

  private var cursorPos = 0
  private var cursorOffset = 0f
  private var scroll = 0f
  private val CursorBlinkTime = 2f
  private var blinkTimer = 0f
  private var text = initialText.toCharArray
  private var textWidth = 0
  private var textChanged = false
  private val events = ArrayBuffer[TextEvent]()
  private val borderAnimation = new ColorAnimation(ColorScheme("TextInputBorder"), 7f)

  override def receiveMouseEvents = true
  override def receiveAllMouseEvents = true

  eventHandlers += {
    case MouseEvent(MouseEvent.State.Release, MouseEvent.Button.Left) =>
      if (isFocused && !clippedBounds.contains(UiHandler.mousePosition))
        unfocus()

    case ClickEvent(MouseEvent.Button.Left, _) =>
      focus()

    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_LEFT, _) if isFocused =>
      events += new CursorMoveLeft
    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_RIGHT, _) if isFocused =>
      events += new CursorMoveRight
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_HOME, _) if isFocused =>
      events += new CursorMoveStart
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_END, _) if isFocused =>
      events += new CursorMoveEnd
    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_BACK, _) if isFocused =>
      events += new EraseCharBack
    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_DELETE, _) if isFocused =>
      events += new EraseCharFront

    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_INSERT, _) if isFocused =>
      for (char <- UiHandler.clipboard.toCharArray)
        events += new WriteChar(char)

    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, Keyboard.KEY_V, ch) if isFocused =>
      if (KeyEvents.isDown(Keyboard.KEY_LCONTROL))
        for (char <- UiHandler.clipboard.toCharArray)
          events += new WriteChar(char)
      else
        events += new WriteChar(ch)

    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_RETURN, _) if isFocused =>
      onConfirm()

    case KeyEvent(KeyEvent.State.Press | KeyEvent.State.Repeat, _, char) if isFocused =>
      if (!char.isControl)
        events += new WriteChar(char)
  }

  def setInput(text: String): Unit = {
    this.text = text.toCharArray
    cursorPos = 0
    cursorOffset = 0
    textWidth = 0
    textChanged = true
  }

  override def minimumSize: Size2D = Size2D(200, 24)
  override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 24)

  def focus(): Unit = {
    if (!isFocused) {
      val color = if (isInputValid(text.mkString)) "TextInputBorderFocused" else "TextInputBorderErrorFocused"
      borderAnimation.goto(ColorScheme(color))
    }
    isFocused = true
    blinkTimer = 0
  }

  def unfocus(): Unit = {
    isFocused = false
    val color = if (isInputValid(text.mkString)) "TextInputBorder" else "TextInputBorderError"
    borderAnimation.goto(ColorScheme(color))
  }

  override def draw(g: Graphics): Unit = {
    if (textWidth == 0f && text.nonEmpty)
      textWidth = text.map(g.font.charWidth(_)).sum

    textChanged = false
    for (event <- events) event.handle(g)
    events.clear()

    if (textChanged) {
      val str = text.mkString
      onInput(str)

      val color = if (isFocused) {
        if (isInputValid(str))
          "TextInputBorderFocused"
        else
          "TextInputBorderErrorFocused"
      } else {
        if (isInputValid(str))
          "TextInputBorder"
        else
          "TextInputBorderError"
      }

      borderAnimation.goto(ColorScheme(color))
    }

    g.rect(bounds, ColorScheme("TextInputBackground"))
    DrawUtils.ring(g, position.x, position.y, size.width, size.height, thickness = 2, borderAnimation.color)

    g.setScissor(position.x + 4, position.y, size.width - 8f, size.height)

    g.background = Color.Transparent
    g.foreground = ColorScheme("TextInputForeground")

    var charOffset = 0
    for (char <- text) {
      g.char(position.x + 8 + charOffset - scroll, position.y + 4, char)
      charOffset += g.font.charWidth(char)
    }

    if (isFocused && blinkTimer < CursorBlinkTime * 0.5f)
      g.rect(position.x + 7 + cursorOffset - scroll, position.y + 4, 2, 16, borderAnimation.color)
  }

  override def update(): Unit = {
    super.update()
    borderAnimation.update()
    blinkTimer = (blinkTimer + UiHandler.dt) % CursorBlinkTime
  }

  private def charWidth(g: Graphics, c: Char): Int = g.font.charWidth(c)

  abstract class TextEvent {
    def handle(g: Graphics): Unit
  }

  class CursorMoveLeft extends TextEvent {
    override def handle(g: Graphics): Unit = {
      if (cursorPos == 0) return

      cursorOffset -= charWidth(g, text(cursorPos - 1))
      cursorPos -= 1
      blinkTimer = 0

      if (cursorOffset < scroll)
        scroll = cursorOffset
    }
  }

  class CursorMoveRight extends TextEvent {
    override def handle(g: Graphics): Unit = {
      if (cursorPos >= text.length) return

      cursorOffset += charWidth(g, text(cursorPos))
      cursorPos += 1
      blinkTimer = 0

      if (cursorOffset - scroll > size.width - 16)
        scroll = cursorOffset - size.width + 16
    }
  }

  class CursorMoveStart extends TextEvent {
    override def handle(g: Graphics): Unit = {
      cursorPos = 0
      cursorOffset = 0
      blinkTimer = 0
      scroll = 0
    }
  }

  class CursorMoveEnd extends TextEvent {
    override def handle(g: Graphics): Unit = {
      cursorPos = text.length
      cursorOffset = textWidth
      blinkTimer = 0
      scroll = (textWidth - size.width + 16).max(0)
    }
  }

  class EraseCharBack extends TextEvent {
    override def handle(g: Graphics): Unit = {
      val (lhs, rhs) = text.splitAt(cursorPos)
      if (lhs.isEmpty) return

      val cw = charWidth(g, lhs.last)
      text = lhs.take(lhs.length - 1) ++ rhs
      textChanged = true
      textWidth -= cw
      cursorOffset -= cw
      cursorPos -= 1
      blinkTimer = 0
      scroll = (scroll - cw).max(0)
    }
  }

  class EraseCharFront extends TextEvent {
    override def handle(g: Graphics): Unit = {
      val (lhs, rhs) = text.splitAt(cursorPos)
      if (rhs.isEmpty) return

      val cw = charWidth(g, rhs.head)
      text = lhs ++ rhs.drop(1)
      textChanged = true
      textWidth -= cw
      blinkTimer = 0

      if (rhs.drop(1).map(charWidth(g, _)).sum < size.width - 16)
        scroll = (scroll - cw).max(0)
    }
  }

  class WriteChar(char: Char) extends TextEvent {
    override def handle(g: Graphics): Unit = {
      val (lhs, rhs) = text.splitAt(cursorPos)
      text = lhs ++ Array(char) ++ rhs
      textChanged = true
      textWidth += charWidth(g, char)
      (new CursorMoveRight).handle(g)
    }
  }
}
