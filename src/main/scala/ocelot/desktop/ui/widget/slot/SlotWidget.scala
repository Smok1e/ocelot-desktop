package ocelot.desktop.ui.widget.slot

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.{Graphics, IconDef}
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, HoverEvent, MouseEvent}
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}

abstract class SlotWidget[T] extends Widget with ClickHandler with HoverHandler {
  def icon: IconDef

  def tierIcon: Option[IconDef] = None

  def itemIcon: Option[IconDef] = None

  def onRemoved(item: T): Unit = {}

  def onAdded(item: T): Unit = {}

  def fillLmbMenu(menu: ContextMenu): Unit = {}

  def fillRmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("Remove", () => item = None))
  }

  def lmbMenuEnabled: Boolean = false

  def rmbMenuEnabled: Boolean = _item.isDefined

  final var _item: Option[T] = None

  final def item_=(v: Option[T]): Unit = {
    if (_item.isDefined)
      onRemoved(_item.get)

    _item = v

    if (v.isDefined)
      onAdded(v.get)
  }

  final def item_=(v: T): Unit = item = Some(v)

  final def item: Option[T] = _item

  final override def minimumSize: Size2D = Size2D(36, 36)
  final override def maximumSize: Size2D = minimumSize

  final override def receiveMouseEvents: Boolean = true

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) if lmbMenuEnabled =>
      val menu = new ContextMenu
      fillLmbMenu(menu)
      root.get.contextMenus.open(menu)
    case ClickEvent(MouseEvent.Button.Right, _) if rmbMenuEnabled =>
      val menu = new ContextMenu
      fillRmbMenu(menu)
      root.get.contextMenus.open(menu)
    case HoverEvent(state) =>
      if (state == HoverEvent.State.Enter)
        onHoverEnter()
      else
        onHoverLeave()
  }

  def onHoverEnter(): Unit = {}

  def onHoverLeave(): Unit = {}

  final override def draw(g: Graphics): Unit = {
    g.sprite("EmptySlot", bounds)

    val iconBounds = bounds.inflate(-2)

    if (itemIcon.isDefined) {
      g.sprite(itemIcon.get, iconBounds)
    } else {
      if (tierIcon.isDefined) g.sprite(tierIcon.get, iconBounds)
      g.sprite(icon, iconBounds)
    }
  }
}
