package ocelot.desktop.ui.widget.slot

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{Label, PaddingBox, Widget}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.tooltip.Tooltip
import ocelot.desktop.util.Orientation
import totoro.ocelot.brain.entity.traits.{DeviceInfo, Entity, Environment, Inventory, Tiered}

import scala.annotation.tailrec

abstract class InventorySlot[T <: Entity](val owner: Inventory#Slot) extends SlotWidget[T] {
  reloadItem()

  override def onAdded(item: T): Unit = owner.put(item)

  override def onRemoved(item: T): Unit = owner.remove()

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    item match {
      case Some(env: Environment) =>
        menu.addEntry(new ContextMenuEntry("Copy address", () => {
          UiHandler.clipboard = env.node.address
        }))
      case _ =>
    }
    super.fillRmbMenu(menu)
  }

  // --------------------------- Tooltip ---------------------------

  private var tooltip: Option[Tooltip] = None

  def tooltipLine1Color: Color = item match {
    case Some(tiered: Tiered) =>
      tiered.tier match {
        case 1 => Color.Yellow
        case 2 => Color.Cyan
        case _ => Color.White
      }
    case _ => Color.White
  }

  final protected def getTooltipSuffixedText(part1: String, part2: String): String = s"$part1 ($part2)"
  final protected def getTooltipTieredText(tier: String): String = s"Tier $tier"
  final protected def getTooltipTieredText(tier: Int): String = getTooltipTieredText((tier + 1).toString)
  final protected def getTooltipDeviceInfoText(deviceInfo: DeviceInfo): String = deviceInfo.getDeviceInfo(DeviceInfo.DeviceAttribute.Description)
  final protected def getTooltipDeviceInfoText(deviceInfo: DeviceInfo, part2: String): String = getTooltipSuffixedText(getTooltipDeviceInfoText(deviceInfo), part2)
  final protected def getTooltipDeviceInfoText(deviceInfo: DeviceInfo, tier: Int): String = getTooltipDeviceInfoText(deviceInfo, getTooltipTieredText(tier))

  def tooltipLine1Text: String = item.get match {
    case deviceInfoTiered: DeviceInfo with Tiered => getTooltipDeviceInfoText(deviceInfoTiered, deviceInfoTiered.tier)
    case _ => ""
  }

  def tooltipChildrenAdder(inner: Widget): Unit = {
    inner.children :+= new Label {
      override def text: String = tooltipLine1Text

      override def color: Color = tooltipLine1Color
    }

    item match {
      case Some(environment: Environment) =>
        inner.children :+= new Label {
          override def text: String = s"Address: ${environment.node.address}"

          override def color: Color = Color.Grey
        }
      case _ =>
    }
  }

  override def onHoverEnter(): Unit = {
    super.onHoverEnter()

    // Showing tooltip only if item is present in slot
    if (item.isEmpty)
      return

    tooltip = Some(new Tooltip())

    val inner: Widget = new Widget {
      override val layout = new LinearLayout(this, orientation = Orientation.Vertical)
    }

    tooltipChildrenAdder(inner)

    tooltip.get.children :+= new PaddingBox(inner, Padding2D.equal(5))

    root.get.tooltipPool.addTooltip(tooltip.get)
  }

  override def onHoverLeave(): Unit = {
    super.onHoverLeave()

    tooltip.foreach(root.get.tooltipPool.closeTooltip(_))
  }

  def reloadItem(): Unit = _item = owner.get.map(_.asInstanceOf[T])
}
