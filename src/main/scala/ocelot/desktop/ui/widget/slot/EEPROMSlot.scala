package ocelot.desktop.ui.widget.slot

import ocelot.desktop.OcelotDesktop.showFileChooserDialog
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.{Label, Widget}
import totoro.ocelot.brain.entity.EEPROM
import totoro.ocelot.brain.entity.traits.Inventory
import totoro.ocelot.brain.loot.Loot

import javax.swing.JFileChooser

class EEPROMSlot(owner: Inventory#Slot) extends InventorySlot[EEPROM](owner) {
  override def itemIcon: Option[IconDef] = item.map(_ => new IconDef("items/EEPROM"))
  override def icon: IconDef = new IconDef("icons/EEPROM")

  override def lmbMenuEnabled: Boolean = true

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("Lua BIOS",
      () => item = Loot.OpenOsEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))
    ))

    menu.addEntry(new ContextMenuEntry("AdvLoader",
      () => item = Loot.AdvLoaderEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))
    ))

    menu.addEntry(new ContextMenuEntry("Empty",
      () => item = new EEPROM,
      icon = Some(new IconDef("items/EEPROM"))
    ))
  }

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    def setSource(): Unit = {
      showFileChooserDialog (
        file => {
          if (file.isDefined)
          item.get.codePath = Some(file.get.toPath)
        },
        JFileChooser.OPEN_DIALOG,
        JFileChooser.FILES_ONLY
      )
    }

    if (item.get.codePath.isDefined) {
      menu.addEntry(new ContextMenuSubmenu("Source file") {
        addEntry(new ContextMenuEntry("Change", setSource))
        addEntry(new ContextMenuEntry("Detach", () => item.get.codeBytes = Some(Array.empty[Byte])))
      })
    }
    else {
      menu.addEntry(new ContextMenuEntry("Attach source file", setSource))
    }

    super.fillRmbMenu(menu)
  }

  override def tooltipLine1Text: String = getTooltipSuffixedText(getTooltipDeviceInfoText(item.get), item.get.label)

  override def tooltipChildrenAdder(inner: Widget): Unit = {
    super.tooltipChildrenAdder(inner)

    if (item.get.codePath.isEmpty)
      return

    inner.children :+= new Label {
      override def text: String = s"Source path: ${item.get.codePath.get.toString}"

      override def color: Color = Color.Grey
    }
  }
}
