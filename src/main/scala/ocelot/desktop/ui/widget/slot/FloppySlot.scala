package ocelot.desktop.ui.widget.slot

import ocelot.desktop.audio.{Audio, SoundSource}
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.FloppyManaged
import totoro.ocelot.brain.entity.traits.{DeviceInfo, Floppy, Inventory}
import totoro.ocelot.brain.loot.Loot
import totoro.ocelot.brain.util.DyeColor

class FloppySlot(owner: Inventory#Slot) extends InventorySlot[Floppy](owner) {
  private val soundFloppyInsert = new SoundSource(Audio.FloppyInsert)
  private val soundFloppyEject = new SoundSource(Audio.FloppyEject)

  override def itemIcon: Option[IconDef] = item.map(fl => new IconDef("items/FloppyDisk_" + fl.color.name))

  override def icon: IconDef = new IconDef("icons/Floppy")

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    for (f <- FloppySlot.FloppyFactories) {
      val floppy = f.create()
      menu.addEntry(new ContextMenuEntry(floppy.label.getLabel,
        () => item = floppy,
        icon = Some(new IconDef("items/FloppyDisk_" + floppy.color.name))))
    }

    menu.addEntry(new ContextMenuEntry("Empty",
      () => item = new FloppyManaged("Floppy Disk", DyeColor.GRAY),
      icon = Some(new IconDef("items/FloppyDisk_dyeGray"))))
  }

  override def lmbMenuEnabled: Boolean = true

  override def onAdded(item: Floppy): Unit = {
    super.onAdded(item)
    soundFloppyInsert.play()
  }

  override def onRemoved(item: Floppy): Unit = {
    super.onRemoved(item)
    soundFloppyEject.play()
  }

  override def tooltipLine1Text: String = item.get.label.getLabel
}

object FloppySlot {
  private val FloppyFactories = Array(Loot.OpenOsFloppy, Loot.Plan9kFloppy, Loot.OPPMFloppy,
    Loot.OpenLoaderFloppy, Loot.NetworkFloppy, Loot.IrcFloppy, Loot.DataFloppy)
}