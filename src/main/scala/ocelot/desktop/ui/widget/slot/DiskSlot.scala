package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.HDDManaged
import totoro.ocelot.brain.entity.traits.{DeviceInfo, Inventory}
import totoro.ocelot.brain.util.Tier

class DiskSlot(owner: Inventory#Slot, val tier: Int) extends InventorySlot[HDDManaged](owner) {
  override def itemIcon: Option[IconDef] = item.map(disk => new IconDef("items/HardDiskDrive" + disk.tier))

  override def icon: IconDef = new IconDef("icons/HDD")
  override def tierIcon: Option[IconDef] = Some(new IconDef("icons/Tier" + tier))

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("HDD (Tier 1)",
      () => item = new HDDManaged(Tier.One),
      icon = Some(new IconDef("items/HardDiskDrive0"))))

    if (tier >= Tier.Two) {
      menu.addEntry(new ContextMenuEntry("HDD (Tier 2)",
        () => item = new HDDManaged(Tier.Two),
        icon = Some(new IconDef("items/HardDiskDrive1"))))
    }

    if (tier >= Tier.Three) {
      menu.addEntry(new ContextMenuEntry("HDD (Tier 3)",
        () => item = new HDDManaged(Tier.Three),
        icon = Some(new IconDef("items/HardDiskDrive2"))))
    }

  }

  override def lmbMenuEnabled: Boolean = true

  override def tooltipLine1Text: String = getTooltipDeviceInfoText(item.get.fileSystem, item.get.tier)
}
