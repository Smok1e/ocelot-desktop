package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconDef
import ocelot.desktop.node.nodes.ComputerNode
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import totoro.ocelot.brain.entity.machine.MachineAPI
import totoro.ocelot.brain.entity.traits.{Entity, GenericCPU, Inventory}
import totoro.ocelot.brain.entity.{APU, CPU}
import totoro.ocelot.brain.util.Tier

class CPUSlot(owner: Inventory#Slot, node: ComputerNode, val tier: Int) extends InventorySlot[GenericCPU with Entity](owner) {
  private val APUAnimation = Some(Array(
    (0, 3f), (1, 3f), (2, 3f), (3, 3f), (4, 3f), (5, 3f),
    (4, 3f), (3, 3f), (2, 3f), (1, 3f), (0, 3f)))

  override def itemIcon: Option[IconDef] = item match {
    case Some(cpu: CPU) => Some(new IconDef("items/CPU" + cpu.tier))
    case Some(apu: APU) => Some(new IconDef("items/APU" + apu.tier, animation = APUAnimation))
    case _ => None
  }

  override def icon: IconDef = new IconDef("icons/CPU")
  override def tierIcon: Option[IconDef] = Some(new IconDef("icons/Tier" + tier))

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("CPU (Tier 1)",
      () => item = new CPU(Tier.One),
      icon = Some(new IconDef("items/CPU0"))))

    if (tier >= Tier.Two) {
      menu.addEntry(new ContextMenuEntry("CPU (Tier 2)",
        () => item = new CPU(Tier.Two),
        icon = Some(new IconDef("items/CPU1"))))
    }

    if (tier >= Tier.Three) {
      menu.addEntry(new ContextMenuEntry("CPU (Tier 3)",
        () => item = new CPU(Tier.Three),
        icon = Some(new IconDef("items/CPU2"))))
    }

    if (tier < Tier.Two) return

    menu.addEntry(new ContextMenuSubmenu("APU (CPU + GPU)", icon = Some(new IconDef("items/GraphicsCard1"))) {
      addEntry(new ContextMenuEntry("Tier 2",
        () => item = new APU(Tier.One),
        icon = Some(new IconDef("items/APU0", animation = APUAnimation))))

      if (tier >= Tier.Three) {
        addEntry(new ContextMenuEntry("Tier 3",
          () => item = new APU(Tier.Two),
          icon = Some(new IconDef("items/APU1", animation = APUAnimation))))

        addEntry(new ContextMenuEntry("Creative",
          () => item = new APU(Tier.Three),
          icon = Some(new IconDef("items/APU2", animation = APUAnimation))))
      }
    })
  }

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    if (item.isEmpty) return

    val cpu = item.get

    menu.addEntry(new ContextMenuSubmenu("Set architecture") {
      for (arch <- cpu.allArchitectures) {
        val name = MachineAPI.getArchitectureName(arch) +
          (if (arch == cpu.architecture) " (current)" else "")

        addEntry(new ContextMenuEntry(name, () => {
          val machine = node.computer.machine
          if (machine.isRunning) {
            machine.stop()
            cpu.setArchitecture(arch)
            machine.start()
          } else {
            cpu.setArchitecture(arch)
          }
        }))
      }
    })

    super.fillRmbMenu(menu)
  }

  override def lmbMenuEnabled: Boolean = true
}
