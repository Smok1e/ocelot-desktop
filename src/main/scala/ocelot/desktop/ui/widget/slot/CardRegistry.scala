package ocelot.desktop.ui.widget.slot

import ocelot.desktop.graphics.IconDef
import totoro.ocelot.brain.entity.traits.{Entity, Tiered}
import totoro.ocelot.brain.entity.{DataCard, GraphicsCard, InternetCard, LinkedCard, NetworkCard, Redstone, WirelessNetworkCard}

import scala.collection.mutable
import scala.reflect.{ClassTag, classTag}

object CardRegistry {
  case class Entry(name: String, tier: Int, icon: IconDef, factory: () => Entity)

  val iconByClass: mutable.HashMap[String, IconDef] = mutable.HashMap()
  val tierByClass: mutable.HashMap[String, Int] = mutable.HashMap()
  val iconByClassAndTier: mutable.HashMap[(String, Int), IconDef] = mutable.HashMap()
  val entries: mutable.ArrayBuffer[Entry] = mutable.ArrayBuffer()

  def addEntry[T <: Entity: ClassTag](name: String, tier: Int, icon: IconDef, factory: () => T): Unit = {
    val entry = Entry(name, tier, icon, factory)
    entries += entry
    val clazz = classTag[T].runtimeClass.getName
    iconByClass.addOne((clazz, icon))
    tierByClass.addOne((clazz, tier))
    iconByClassAndTier.addOne(((clazz, entry.tier), icon))
  }

  def getIcon(entity: Entity): Option[IconDef] = {
    val clazz = entity.getClass.getName
    entity match {
      case t: Tiered => iconByClassAndTier.get((clazz, t.tier))
      case _ => iconByClass.get(clazz)
    }
  }

  def getTier(entity: Entity): Int = {
    val clazz = entity.getClass.getName
    entity match {
      case t: Tiered => t.tier
      case _ => tierByClass.getOrElse(clazz, 0)
    }
  }

  addEntry("Graphics Card", 0, new IconDef("items/GraphicsCard0"), () => new GraphicsCard(0))
  addEntry("Graphics Card", 1, new IconDef("items/GraphicsCard1"), () => new GraphicsCard(1))
  addEntry("Graphics Card", 2, new IconDef("items/GraphicsCard2"), () => new GraphicsCard(2))

  addEntry("Network Card", 0, new IconDef("items/NetworkCard"),
    () => new NetworkCard)

  addEntry("Wireless Net. Card", 0, new IconDef("items/WirelessNetworkCard0"),
    () => new WirelessNetworkCard.Tier1)
  addEntry("Wireless Net. Card", 1, new IconDef("items/WirelessNetworkCard1"),
    () => new WirelessNetworkCard.Tier2)

  private val LinkedCardAnimation = Some(Array((0, 3f), (1, 3f), (2, 3f), (3, 3f), (4, 3f), (5, 3f)))

  addEntry("Linked Card", 2, new IconDef("items/LinkedCard", animation = LinkedCardAnimation),
    () => new LinkedCard)

  private val InternetCardAnimation = Some(Array((0, 2f), (1, 7f), (0, 5f), (1, 4f), (0, 7f), (1, 2f), (0, 8f), (1, 9f), (0, 6f), (1, 4f)))

  addEntry("Internet Card", 1, new IconDef("items/InternetCard", animation = InternetCardAnimation),
    () => new InternetCard)

  addEntry("Redstone Card", 0, new IconDef("items/RedstoneCard0"), () => new Redstone.Tier1)
  addEntry("Redstone Card", 1, new IconDef("items/RedstoneCard1"), () => new Redstone.Tier2)

  private val DataCardAnimation = Some(Array((0, 4f), (1, 4f), (2, 4f), (3, 4f), (4, 4f), (5, 4f), (6, 4f), (7, 4f)))

  addEntry("Data Card", 0, new IconDef("items/DataCard0", animation = DataCardAnimation),
    () => new DataCard.Tier1)
  addEntry("Data Card", 1, new IconDef("items/DataCard1", animation = DataCardAnimation),
    () => new DataCard.Tier2)
  addEntry("Data Card", 2, new IconDef("items/DataCard2", animation = DataCardAnimation),
    () => new DataCard.Tier3)
}

