package ocelot.desktop.ui.widget

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.tooltip.Tooltip
import ocelot.desktop.util.Orientation

class LabelTooltip(text: String) extends Tooltip {
  override val DelayTime: Float = 1.0f

  private val inner: Widget = new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)
  }

  for (line <- text.split("\n")) {
    inner.children :+= new Label {
      override def text: String = line
      override def color: Color = Color.White
    }
  }

  children :+= new PaddingBox(inner, Padding2D.equal(4))

  def onSaveSelected(): Unit = {}

  def onExitSelected(): Unit = {}
}
