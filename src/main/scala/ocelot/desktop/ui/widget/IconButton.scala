package ocelot.desktop.ui.widget

import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, HoverEvent, MouseEvent}
import ocelot.desktop.util.Spritesheet
import ocelot.desktop.util.animation.{ColorAnimation, ValueAnimation}

class IconButton(releasedIcon: String, pressedIcon: String,
                 releasedColor: Color = Color.White,
                 pressedColor: Color = Color.White,
                 sizeMultiplier: Float = 1,
                 isSwitch: Boolean = false) extends Widget with ClickHandler with HoverHandler
{

  private val speed = 10f
  private val colorAnimation = new ColorAnimation(pressedColor, speed)
  private val alphaAnimation = new ValueAnimation(0f, speed)

  private var _isOn = false

  override def receiveMouseEvents = true

  eventHandlers += {
    case HoverEvent(state) =>
      if (state == HoverEvent.State.Enter) onHoverEnter()
      else onHoverLeave()
    case MouseEvent(MouseEvent.State.Press, MouseEvent.Button.Left) =>
      if (!isSwitch || !isOn) press() else release()
    case ClickEvent(MouseEvent.Button.Left, _) =>
      if (!isSwitch || !isOn) release() else press()
  }

  def isOn: Boolean = _isOn

  def onPressed(): Unit = {}

  def onReleased(): Unit = {}

  def onHoverEnter(): Unit = {}

  def onHoverLeave(): Unit = {}

  def press(): Unit = {
    colorAnimation.goto(pressedColor)
    alphaAnimation.goto(1f)
    _isOn = true
    onPressed()
  }

  def release(): Unit = {
    colorAnimation.goto(releasedColor)
    alphaAnimation.goto(0f)
    _isOn = false
    onReleased()
  }

  size = minimumSize

  private def releasedIconSize: Size2D = Spritesheet.spriteSize(releasedIcon) * sizeMultiplier

  private def pressedIconSize: Size2D = Spritesheet.spriteSize(pressedIcon) * sizeMultiplier

  override def minimumSize: Size2D = releasedIconSize.max(pressedIconSize)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    if (alphaAnimation.value < 1f)
      g.sprite(releasedIcon, position + (size - releasedIconSize) / 2f, releasedIconSize,
        releasedColor.toRGBANorm.mapA(_ * (1f - alphaAnimation.value)))

    if (alphaAnimation.value > 0f)
      g.sprite(pressedIcon, position + (size - pressedIconSize) / 2f, pressedIconSize,
        pressedColor.toRGBANorm.mapA(_ * alphaAnimation.value))
  }

  override def update(): Unit = {
    super.update()
    colorAnimation.update()
    alphaAnimation.update()
    if (isOn && !_isOn) press()
    if (!isOn && _isOn) release()
  }
}
