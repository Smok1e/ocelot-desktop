package ocelot.desktop.ui.widget.verticalmenu

import ocelot.desktop.ColorScheme
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.Orientation

class VerticalMenu extends Widget {
  override def receiveMouseEvents: Boolean = true

  private val container: Widget = new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)
  }

  children :+= container

  private var _entries: Array[VerticalMenuButton] = Array()
  def entries: Array[VerticalMenuButton] = _entries

  def addEntry(button: VerticalMenuButton): VerticalMenu = {
    container.children :+= button
    _entries :+= button
    this
  }

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("VerticalMenuBackground"))
    drawChildren(g)
  }
}
