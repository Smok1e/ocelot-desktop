package ocelot.desktop.ui.widget

import ocelot.desktop.ui.layout.Layout

class NoLayoutBox extends Widget {
  override protected val layout = new Layout(this)
}
