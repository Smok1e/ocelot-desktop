package ocelot.desktop.util

trait Resource {
  def initResource(): Unit = {}
  def freeResource(): Unit
}
