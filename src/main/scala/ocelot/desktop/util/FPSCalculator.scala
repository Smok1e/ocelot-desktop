package ocelot.desktop.util

class FPSCalculator {
  private var prevTime = System.currentTimeMillis()
  private var prevFrameTime = System.currentTimeMillis()
  private var numFrames = 0

  private var _fps: Float = 0f

  var dt: Float = 0

  def fps: Float = _fps

  def tick(): Unit = {
    val currentTime = System.currentTimeMillis()
    dt = (currentTime - prevFrameTime) / 1000f

    numFrames += 1

    if (currentTime - prevTime > 1000) {
      val delta = currentTime - prevTime
      prevTime = currentTime
      _fps = numFrames.asInstanceOf[Float] / delta * 1000f
      numFrames = 0
    }

    prevFrameTime = currentTime
  }
}
