package ocelot.desktop

import java.util.concurrent.locks.Lock

package object util {
  final def withLockAcquired[T](lock: Lock, f: () => T): T = {
    lock.lock()

    try {
      f()
    } finally {
      lock.unlock()
    }
  }
}
