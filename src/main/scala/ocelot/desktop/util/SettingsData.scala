package ocelot.desktop.util

import ocelot.desktop.Settings.Int2D

class SettingsData {
  def this(data: SettingsData) {
    this()
    updateWith(data)
  }

  var volumeMaster: Float = 1f
  var volumeBeep: Float = 1f
  var volumeEnvironment: Float = 1f

  var windowSize: Int2D = new Int2D()
  var windowPosition: Int2D = new Int2D()
  var windowFullscreen: Boolean = false

  var recentWorkspace: Option[String] = None

  var stickyWindows: Boolean = true

  def updateWith(data: SettingsData): Unit = {
    // TODO: maybe apply some automated mapping solution
    this.volumeMaster = data.volumeMaster
    this.volumeBeep = data.volumeBeep
    this.volumeEnvironment = data.volumeEnvironment

    this.windowPosition = data.windowPosition
    this.windowSize = data.windowSize
    this.windowFullscreen = data.windowFullscreen

    this.stickyWindows = data.stickyWindows
  }
}
