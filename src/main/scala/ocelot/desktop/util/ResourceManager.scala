package ocelot.desktop.util

import scala.collection.mutable.ArrayBuffer

object ResourceManager {
  private var initialized = false
  private val resources = new ArrayBuffer[Resource]

  def registerResource(resource: Resource): Unit = {
    resources += resource
    if (initialized) resource.initResource()
  }

  def initResources(): Unit = {
    if (!initialized) {
      for (resource <- resources)
        resource.initResource()
      initialized = true
    }
  }

  def forEach(predicate: Resource => Unit): Unit = {
    resources.foreach(predicate)
  }

  def freeResource(resource: Resource): Unit = {
    resource.freeResource()
    resources -= resource
  }

  def freeResources(): Unit = {
    for (resource <- resources)
      resource.freeResource()

    resources.clear()
  }
}
