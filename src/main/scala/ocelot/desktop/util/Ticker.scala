package ocelot.desktop.util

import java.util.concurrent.TimeUnit

class Ticker extends Logging {
  var lastTick: Long = System.nanoTime()
  var tick: Long = 0

  private[this] var _tickInterval: Long = _

  def tickInterval: Long = _tickInterval / 1000

  def tickInterval_=(us: Float): Unit = {
    logger.info(f"Setting tick interval to $us us (${1000000f / us} s^-1)")
    _tickInterval = (us * 1000).toLong
  }

  tickInterval = 50000

  def waitNext(): Unit = {
    val deadline = lastTick + _tickInterval
    var time = System.nanoTime()
    while (time < deadline) {
      val rem = deadline - time
      if (rem > 1000000)
        TimeUnit.NANOSECONDS.sleep(rem - 1000000)
      else
        Thread.`yield`()
      time = System.nanoTime()
    }

    lastTick = System.nanoTime()
    tick += 1
  }
}
