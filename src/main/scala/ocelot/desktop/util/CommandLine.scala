package ocelot.desktop.util

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object CommandLine {
  val Help: Argument = Argument("-h", "--help", "Show this help")
  val ConfigPath: Argument = Argument("-c", "--config", "Provide path to custom ocelot.conf file", takesValue = true)
  val WorkspacePath: Argument = Argument("-w", "--workspace", "Open provided workspace on start", takesValue = true)

  val availableArguments: ArrayBuffer[Argument] = ArrayBuffer(
    Help, ConfigPath, WorkspacePath
  )

  def parse(args: Array[String]): mutable.HashMap[Argument, Option[String]] = {
    val arguments = new mutable.HashMap[Argument, Option[String]]()
    val iterator = args.iterator
    while (iterator.hasNext) {
      val value = iterator.next()
      availableArguments.foreach(arg => {
        if (value == arg.shortForm || value == arg.longForm) {
          arguments.put(arg, if (arg.takesValue) iterator.nextOption() else None)
        }
      })
    }
    arguments
  }

  def doc: String = """
      |Usage: java -jar ocelot.jar [options]
      |where options include:
      |""".stripMargin + availableArguments.foldLeft("") {
    (acc: String, arg: Argument) => s"$acc\n    ${arg.shortForm}, ${arg.longForm}: ${arg.doc}"
  }

  case class Argument(shortForm: String, longForm: String, doc: String, takesValue: Boolean = false)
}
