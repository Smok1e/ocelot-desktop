package ocelot.desktop.util

object MathUtils {
  implicit class ExtendedFloat(val x: Float) {
    def clamp(a: Float, b: Float): Float = x.max(a).min(b)
  }
}
