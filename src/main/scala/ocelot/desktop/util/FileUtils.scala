package ocelot.desktop.util

import org.apache.commons.lang3.SystemUtils

import java.io.{IOException, RandomAccessFile}
import java.nio.ByteBuffer
import java.nio.file.{Path, Paths}

object FileUtils extends Logging {
  def load(filename: String): ByteBuffer = {
    val file = new RandomAccessFile(filename, "r")
    val inChannel = file.getChannel

    try {
      val fileSize = inChannel.size
      val buffer: ByteBuffer = ByteBuffer.allocate(fileSize.toInt)
      inChannel.read(buffer)
      buffer.flip
      buffer
    } catch {
      case e: IOException =>
        logger.error(s"Cannot load file: $filename", e)
        null
    } finally {
      if (file != null) file.close()
      if (inChannel != null) inChannel.close()
    }
  }

  def getOcelotConfigDirectory: Path = {
    if (SystemUtils.IS_OS_WINDOWS) Paths.get(System.getenv("APPDATA"), "Ocelot")
    else Paths.get(System.getProperty("user.home"), ".config", "ocelot")
  }
}
