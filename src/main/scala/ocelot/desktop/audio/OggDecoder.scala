package ocelot.desktop.audio

import com.jcraft.jogg.{Packet, Page, StreamState, SyncState}
import com.jcraft.jorbis.{Block, Comment, DspState, Info}
import ocelot.desktop.util.Logging

import java.io.{ByteArrayOutputStream, IOException, InputStream}
import java.nio.{ByteBuffer, ByteOrder}
import scala.util.control.Breaks._

/**
  * Source code adapted from https://github.com/lsliwko/jorbis-oggdecoder project
  */

class OggDecoder() extends Logging {
  /** The conversion buffer size */
  private var convSize: Int = 4096 * 2

  /** The buffer used to read OGG file */
  private val convBuffer: Array[Byte] = new Array[Byte](convSize) // take 8k out of the data segment, not the stack

  /**
    * Get the data out of an OGG file
    *
    * @param input The input stream from which to read the OGG file
    * @return The data describing the OGG that's been read
    */
  @throws[IOException]
  def getData(input: InputStream): OggData = {
    // the following code come from an example in the Java OGG library.
    // it is extremely complicated and a good example of a library
    // that's potentially too low level for average users. I'd suggest
    // accepting this code as working and not thinking too hard
    // on what its actually doing
    val dataOut: ByteArrayOutputStream = new ByteArrayOutputStream()
    val oy: SyncState = new SyncState // sync and verify incoming physical bitstream
    val os: StreamState = new StreamState // take physical pages, weld into a logical stream of packets
    val og: Page = new Page // one Ogg bitstream page.  Vorbis packets are inside
    val op: Packet = new Packet // one raw packet of data for decode

    val vi: Info = new Info // struct that stores all the static vorbis bitstream settings
    val vc: Comment = new Comment // struct that stores all the bitstream user comments
    val vd: DspState = new DspState // central working state for the packet -> PCM decoder
    val vb: Block = new Block(vd) // local working space for packet -> PCM decode

    var buffer: Array[Byte] = null
    var bytes: Int = 0

    val bigEndian: Boolean = ByteOrder.nativeOrder == ByteOrder.BIG_ENDIAN

    // Decode setup
    oy.init() // Now we can read pages

    breakable {
      while (true) { // we repeat if the bitstream is chained
        var eos: Int = 0

        // grab some data at the head of the stream.
        // we want the first page (which is guaranteed to be small and only contain the Vorbis
        // stream initial header).
        // we need the first page to get the stream serialno.
        // submit a 4k block to libvorbis' Ogg layer
        var index: Int = oy.buffer(4096)
        buffer = oy.data
        try {
          bytes = input.read(buffer, index, 4096)
        }
        catch {
          case e: Exception => throw new IOException(e.getMessage)
        }
        oy.wrote(bytes)

        // get the first page.
        if (oy.pageout(og) != 1) { // have we simply run out of data? if so, we're done
          if (bytes < 4096) {
            break
          }
          // error case (must not be Vorbis data)
          throw new IOException("Input does not appear to be an Ogg bitstream.")
        }

        // get the serial number and set up the rest of decode.
        // serialno first; use it to set up a logical stream
        os.init(og.serialno)

        // extract the initial header from the first page and verify that the
        // Ogg bitstream is in fact Vorbis data
        // I handle the initial header first instead of just having the code
        // read all three Vorbis headers at once because reading the initial
        // header is an easy way to identify a Vorbis bitstream and it's
        // useful to see that functionality separated out.
        vi.init()
        vc.init()
        if (os.pagein(og) < 0) { // error; stream version mismatch perhaps
          throw new IOException("Error reading first page of Ogg bitstream data.")
        }

        if (os.packetout(op) != 1) { // no page? must not be Vorbis
          throw new IOException("Error reading initial header packet.")
        }

        if (vi.synthesis_headerin(vc, op) < 0) { // error case; not a Vorbis header
          throw new IOException("This Ogg bitstream does not contain Vorbis audio data.")
        }

        // at this point, we're sure we're Vorbis. we've set up the logical
        // (Ogg) bitstream decoder. get the comment and codebook headers and
        // set up the Vorbis decoder
        // the next two packets in order are the comment and codebook headers.
        // They're likely large and may span multiple pages.  Thus we read
        // and submit data until we get our two packets, watching that no
        // pages are missing.  If a page is missing, error out; losing a
        // header page is the only place where missing data is fatal. */
        var i: Int = 0
        while (i < 2) {
          breakable {
            while (i < 2) {
              var result: Int = oy.pageout(og)
              if (result == 0) {
                break
                // need more data
              }

              // Don't complain about missing or corrupt data yet.  We'll
              // catch it at the packet output phase
              if (result == 1) {
                os.pagein(og) // we can ignore any errors here

                // as they'll also become apparent at packetout
                breakable {
                  while (i < 2) {
                    result = os.packetout(op)
                    if (result == 0) {
                      break
                    }
                    if (result == -1) { // uh oh; data at some point was corrupted or missing!
                      // we can't tolerate that in a header. die.
                      throw new IOException("Corrupt secondary header.  Exiting.")
                    }
                    vi.synthesis_headerin(vc, op)
                    i += 1
                  }
                }
              }
            }
          }

          // no harm in not checking before adding more
          index = oy.buffer(4096)
          buffer = oy.data

          try {
            bytes = input.read(buffer, index, 4096)
          }
          catch {
            case e: Exception => throw new IOException(e.getMessage)
          }

          if (bytes == 0 && i < 2) {
            throw new IOException("End of file before finding all Vorbis headers!")
          }

          oy.wrote(bytes)
        }

        convSize = 4096 / vi.channels

        // OK, got and parsed all three headers. initialize the Vorbis packet -> PCM decoder
        vd.synthesis_init(vi) // central decode state

        vb.init(vd) // local state for most of the decode

        // so multiple block decodes can proceed in parallel
        // we could init multiple vorbis_block structures for vd here
        val _pcm: Array[Array[Array[Float]]] = new Array[Array[Array[Float]]](1)
        val _index: Array[Int] = new Array[Int](vi.channels)

        // the rest is just a straight decode loop until end of stream
        while (eos == 0) {
          breakable {
            while (eos == 0) {
              var result: Int = oy.pageout(og)
              if (result == 0) {
                break
                // need more data
              }
              if (result == -1) { // missing or corrupt data at this page position
                logger.debug("OGG Decoder: Corrupt or missing data in bitstream; continuing...")
              }
              else {
                os.pagein(og) // can safely ignore errors at

                // this point
                breakable {
                  while (true) {
                    result = os.packetout(op)

                    if (result == 0) {
                      break
                    }

                    if (result == -1) {
                      // no reason to complain; already complained above
                    }
                    else { // we have a packet. decode it
                      var samples: Int = 0

                      if (vb.synthesis(op) == 0) { // test for success!
                        vd.synthesis_blockin(vb)
                      }

                      // **pcm is a multichannel float vector. in stereo, for
                      // example, pcm[0] is left, and pcm[1] is right. samples is
                      // the size of each channel. convert the float values
                      // (-1.<=range<=1.) to whatever PCM format and write it out
                      while ( {
                        samples = vd.synthesis_pcmout(_pcm, _index)
                        samples
                      } > 0) {
                        val pcm: Array[Array[Float]] = _pcm(0)
                        var clipFlag: Boolean = false
                        val bout: Int = if (samples < convSize) samples else convSize

                        // convert floats to 16 bit signed ints (host order) and
                        // interleave
                        i = 0
                        while (i < vi.channels) {
                          var ptr: Int = i * 2
                          val mono: Int = _index(i)

                          for (j <- 0 until bout) {
                            var value: Int = (pcm(i)(mono + j) * 32767.0).toInt

                            // might as well guard against clipping
                            if (value > 32767) {
                              value = 32767
                              clipFlag = true
                            }

                            if (value < -32768) {
                              value = -32768
                              clipFlag = true
                            }

                            if (value < 0) {
                              value = value | 0x8000
                            }

                            if (bigEndian) {
                              convBuffer(ptr) = (value >>> 8).toByte
                              convBuffer(ptr + 1) = value.toByte
                            }
                            else {
                              convBuffer(ptr) = value.toByte
                              convBuffer(ptr + 1) = (value >>> 8).toByte
                            }

                            ptr += 2 * vi.channels
                          }

                          i += 1
                        }

                        dataOut.write(convBuffer, 0, 2 * vi.channels * bout)
                        vd.synthesis_read(bout) // tell libvorbis how

                        // many samples we
                        // actually consumed
                      }
                    }
                  }
                }

                if (og.eos != 0) {
                  eos = 1
                }
              }
            }
          }

          if (eos == 0) {
            index = oy.buffer(4096)
            buffer = oy.data
            try {
              bytes = input.read(buffer, index, 4096)
            }
            catch {
              case e: Exception => throw new IOException(e.getMessage)
            }
            oy.wrote(bytes)
            if (bytes == 0) {
              eos = 1
            }
          }
        }

        // clean up this logical bitstream; before exit we see if we're
        // followed by another [chained]
        os.clear()

        // ogg_page and ogg_packet structs always point to storage in libvorbis
        // they're never freed or manipulated directly
        vb.clear()
        vd.clear()
        vi.clear() // must be called last
      }
    }

    // OK, clean up the framer
    oy.clear()

    val ogg: OggData = new OggData
    ogg.channels = vi.channels
    ogg.rate = vi.rate

    val data: Array[Byte] = dataOut.toByteArray
    ogg.data = ByteBuffer.allocateDirect(data.length)
    ogg.data.put(data)
    ogg.data.rewind()
    ogg
  }
}
