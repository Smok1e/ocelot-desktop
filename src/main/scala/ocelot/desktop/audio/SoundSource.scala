package ocelot.desktop.audio

import ocelot.desktop.Settings
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.util.{Logging, Resource, ResourceManager}
import org.lwjgl.openal.AL10

class SoundSource(soundBuffer: SoundBuffer, looping: Boolean = false) extends Resource with Logging {
  private var sourceId: Int = -1

  ResourceManager.registerResource(this)

  override def initResource(): Unit = {
    if (!UiHandler.audioDisabled) {
      if (soundBuffer.getBufferId != -1) {
        sourceId = AL10.alGenSources()
        AL10.alSourcei(sourceId, AL10.AL_BUFFER, soundBuffer.getBufferId)
        AL10.alSourcef(sourceId, AL10.AL_PITCH, 1f)
        AL10.alSourcef(sourceId, AL10.AL_GAIN, Settings.get.volumeEnvironment * Settings.get.volumeMaster)
        AL10.alSource3f(sourceId, AL10.AL_POSITION, 0f, 0f, 0f)
        AL10.alSourcei(sourceId, AL10.AL_LOOPING, if (looping) AL10.AL_TRUE else AL10.AL_FALSE)
      } else {
        logger.error("Unable to create sound source: sound buffer is missing!")
      }
    }
  }

  def getSourceId: Int = sourceId

  def getStatus: Int = if (sourceId == -1) -1 else AL10.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE)
  def isPlaying: Boolean = getStatus == AL10.AL_PLAYING
  def isPaused: Boolean = getStatus == AL10.AL_PAUSED
  def isStopped: Boolean = getStatus == AL10.AL_STOPPED

  def play(): Unit = if (!UiHandler.audioDisabled && !isPlaying) AL10.alSourcePlay(sourceId)
  def pause(): Unit = if (!UiHandler.audioDisabled && isPlaying) AL10.alSourcePause(sourceId)
  def stop(): Unit = if (!UiHandler.audioDisabled && isPlaying || isPaused) AL10.alSourceStop(sourceId)

  def setVolume(value: Float): Unit = {
    if (sourceId != -1) {
      AL10.alSourcef(sourceId, AL10.AL_GAIN, value)
    }
  }

  override def freeResource(): Unit = {
    if (sourceId != -1) {
      stop()
      AL10.alDeleteSources(sourceId)
    }
  }
}
