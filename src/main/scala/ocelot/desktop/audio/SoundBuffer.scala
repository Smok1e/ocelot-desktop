package ocelot.desktop.audio

import ocelot.desktop.ui.UiHandler
import ocelot.desktop.util.{FileUtils, Logging, Resource, ResourceManager}
import org.lwjgl.openal.AL10

class SoundBuffer(val file: String) extends Resource with Logging {
  private var bufferId: Int = -1

  ResourceManager.registerResource(this)

  override def initResource(): Unit = {
    if (!UiHandler.audioDisabled) {
      logger.debug(s"Loading sound buffer from '$file'...")
      bufferId = AL10.alGenBuffers()

      if (AL10.alIsExtensionPresent("AL_EXT_vorbis")) {
        val fileBuffer = FileUtils.load(file)
        if (fileBuffer != null) {
          AL10.alBufferData(bufferId, AL10.AL_FORMAT_VORBIS_EXT, fileBuffer, -1)
        } else {
          logger.error(s"Could not load '$file'!")
        }
      } else {
        val oggDecoder = new OggDecoder()
        val oggData = oggDecoder.getData(getClass.getResourceAsStream(file))
        AL10.alBufferData(bufferId,
          if (oggData.channels > 1) AL10.AL_FORMAT_STEREO16
          else AL10.AL_FORMAT_MONO16, oggData.data, oggData.rate)
        if (AL10.alGetError != AL10.AL_NO_ERROR) {
          logger.error(s"Could not load '$file'!")
        }
      }
    }
  }

  def getBufferId: Int = bufferId

  def freeResource(): Unit = {
    if (bufferId != -1) {
      AL10.alDeleteBuffers(bufferId)
      logger.debug(s"Destroyed sound buffer (ID: $bufferId) loaded from $file")
    }
  }
}
