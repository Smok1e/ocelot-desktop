package ocelot.desktop.audio

import java.nio.ByteBuffer

/**
  * Source code adapted from https://github.com/lsliwko/jorbis-oggdecoder project
  */

class OggData {
  /** The data that has been read from the OGG file */
  var data: ByteBuffer = _
  /** The sampling rate */
  var rate = 0
  /** The number of channels in the sound file */
  var channels = 0
}
