package ocelot.desktop

import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions, ConfigValueFactory}
import ocelot.desktop.Settings.ExtendedConfig
import ocelot.desktop.util.{Logging, SettingsData}
import org.apache.commons.lang3.SystemUtils

import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path}
import java.util
import scala.io.{Codec, Source}

class Settings(val config: Config) extends SettingsData {
  volumeMaster = (config.getDouble("ocelot.sound.volumeMaster") max 0 min 1).toFloat
  volumeBeep = (config.getDouble("ocelot.sound.volumeBeep") max 0 min 1).toFloat
  volumeEnvironment = (config.getDouble("ocelot.sound.volumeEnvironment") max 0 min 1).toFloat

  windowSize = config.getInt2D("ocelot.window.size")
  windowPosition = config.getInt2D("ocelot.window.position")
  windowFullscreen = config.getBooleanOrElse("ocelot.window.fullscreen", default = false)

  // Windows uses life-hack when it sets position to (-8,-8) for maximized windows to hide the frame.
  // (https://devblogs.microsoft.com/oldnewthing/20150304-00/?p=44543)
  // In LWJGL 2 we cannot read the "maximize" flag from the window, so we will just adjust the position.
  if (SystemUtils.IS_OS_WINDOWS && windowPosition.x == -8 && windowPosition.y == -8) {
    windowPosition.x = 0
    windowPosition.y = 0
    windowSize.x -= 16
    windowSize.y -= 16
  }

  recentWorkspace = config.getOptionalString("ocelot.workspace.recent")
  stickyWindows = config.getBooleanOrElse("ocelot.workspace.stickyWindows", default = true)
}

object Settings extends Logging {
  implicit class ExtendedConfig(val config: Config) {
    def getInt2D(path: String): Int2D =
      if (config.hasPath(path)) new Int2D(config.getIntList(path))
      else new Int2D()

    def getOptionalString(path: String): Option[String] =
      if (config.hasPath(path)) Option(config.getString(path))
      else None

    def getBooleanOrElse(path: String, default: Boolean): Boolean =
      if (config.hasPath(path)) config.getBoolean(path)
      else default

    def withValuePreserveOrigin(path: String, value: Any): Config =
      config.withValue(path,
        if (config.hasPath(path))
          ConfigValueFactory.fromAnyRef(value).withOrigin(config.getValue(path).origin())
        else ConfigValueFactory.fromAnyRef(value)
      )

    def withValue(path: String, value: Int2D): Config = {
      config.withValue(path, ConfigValueFactory.fromIterable(util.Arrays.asList(value.x, value.y)))
    }

    def withValue(path: String, value: Option[Any]): Config =
      config.withValue(path, ConfigValueFactory.fromAnyRef(value.orNull))
  }

  class Int2D(var x: Int, var y: Int) {
    var isSet: Boolean = false

    def this() = this(0, 0)
    def this(list: util.List[Integer]) = {
      this()
      if (list.size() == 2) {
        set(list.get(0), list.get(1))
        isSet = true
      }
    }

    def set(x: Int, y: Int): Unit = {
      this.x = x
      this.y = y
      isSet = true
    }
  }

  private val renderOptions = ConfigRenderOptions.defaults().setJson(false).setOriginComments(false)
  private var settings: Settings = _
  def get: Settings = settings

  def load(path: Path): Unit = {
    import java.lang.System.{lineSeparator => EOL}
    val defaults = {
      val in = getClass.getResourceAsStream("/ocelot/desktop/ocelot.conf")
      val config = Source.fromInputStream(in)(Codec.UTF8).getLines().mkString("", EOL, EOL)
      in.close()
      ConfigFactory.parseString(config)
    }
    var stream: InputStream = null
    try {
      stream = Files.newInputStream(path)
      val source = Source.fromInputStream(stream)(Codec.UTF8)
      val plain = source.getLines().mkString("", EOL, EOL)
      val config = ConfigFactory.parseString(plain)
      settings = new Settings(config)
      source.close()
      logger.info(s"Loaded Ocelot Desktop configuration from: $path")
    }
    catch {
      case e: Throwable =>
        if (Files.exists(path)) {
          logger.warn("Failed loading config, using defaults.", e)
        }
        settings = new Settings(defaults)
    }
    finally {
      if (stream != null)
        stream.close()
    }
  }

  def save(path: Path): Unit = {
    if (settings != null) {
      val updatedConfig = settings.config
        .withValuePreserveOrigin("ocelot.sound.volumeMaster", settings.volumeMaster)
        .withValuePreserveOrigin("ocelot.sound.volumeBeep", settings.volumeBeep)
        .withValuePreserveOrigin("ocelot.sound.volumeEnvironment", settings.volumeEnvironment)
        .withValue("ocelot.window.position", settings.windowPosition)
        .withValue("ocelot.window.size", settings.windowSize)
        .withValuePreserveOrigin("ocelot.window.fullscreen", settings.windowFullscreen)
        .withValue("ocelot.workspace.recent", settings.recentWorkspace)
        .withValuePreserveOrigin("ocelot.workspace.stickyWindows", settings.stickyWindows)

      if (!Files.exists(path.getParent))
        Files.createDirectory(path.getParent)

      Files.write(path, updatedConfig.root().render(renderOptions).getBytes(StandardCharsets.UTF_8))
      logger.info(s"Saved Ocelot Desktop configuration to: $path")
    }
  }
}
