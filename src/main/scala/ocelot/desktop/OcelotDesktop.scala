package ocelot.desktop

import buildinfo.BuildInfo
import li.flor.nativejfilechooser.NativeJFileChooser
import ocelot.desktop.audio.{Audio, SoundSource}
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.swing.SplashScreen
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.settings.SettingsDialog
import ocelot.desktop.util.CommandLine.Argument
import ocelot.desktop.util.FileUtils.getOcelotConfigDirectory
import ocelot.desktop.util._
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.LogManager
import totoro.ocelot.brain.Ocelot
import totoro.ocelot.brain.event.FileSystemActivityType.Floppy
import totoro.ocelot.brain.event._
import totoro.ocelot.brain.nbt.ExtendedNBT.{extendNBTTagCompound, extendNBTTagList}
import totoro.ocelot.brain.nbt.{CompressedStreamTools, NBT, NBTTagCompound, NBTTagString}
import totoro.ocelot.brain.user.User
import totoro.ocelot.brain.workspace.Workspace

import java.io._
import java.nio.file.{Files, Path, Paths}
import java.util.concurrent.locks.{Lock, ReentrantLock}
import javax.swing.JFileChooser
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Random, Success, Try}

object OcelotDesktop extends Logging {
  val splashScreen = new SplashScreen()
  var root: RootWidget = _
  val players: ArrayBuffer[User] = ArrayBuffer(User("myself"))
  val tpsCounter = new FPSCalculator
  val ticker = new Ticker

  private val tickLock: Lock = new ReentrantLock()

  private val random: Random = new Random()

  def withTickLockAcquired(f: () => Unit): Unit = withLockAcquired(tickLock, f)

  def mainInner(args: mutable.HashMap[Argument, Option[String]]): Unit = {
    logger.info("Starting up Ocelot Desktop")
    logger.info(s"Version: ${BuildInfo.version} (${BuildInfo.commit.take(7)})")
    splashScreen.setStatus("Initializing brain...", 0.10f)
    Ocelot.initialize(LogManager.getLogger(Ocelot))

    splashScreen.setStatus("Loading configuration...", 0.20f)
    val customConfigPath = args.get(CommandLine.ConfigPath).flatten
    val settingsFile = if (customConfigPath.isEmpty)
      getOcelotConfigDirectory.resolve("ocelot.conf")
    else
      Paths.get(customConfigPath.get)
    Settings.load(settingsFile)
    ColorScheme.load(Source.fromURL(getClass.getResource("/ocelot/desktop/colorscheme.txt")))

    splashScreen.setStatus("Initializing GUI...", 0.30f)
    createWorkspace()

    val loadRecentWorkspace = Settings.get.recentWorkspace.isDefined
    root = new RootWidget(!loadRecentWorkspace)

    UiHandler.init(root)

    splashScreen.setStatus("Loading resources...", 0.60f)
    // loading resources _after_ the UiHandler was initialized, because the native libraries are not available before
    ResourceManager.initResources()

    setupEventHandlers()

    splashScreen.setStatus("Loading workspace...", 0.90f)
    val workspaceToOpen = args.get(CommandLine.WorkspacePath).flatten
    if (loadRecentWorkspace || workspaceToOpen.isDefined) {
      val file = workspaceToOpen.orElse(Settings.get.recentWorkspace).map(new File(_))
      load(file) match {
        case Failure(exception) =>
          val errorMessage = if (workspaceToOpen.isDefined)
            "Could not open the specified workspace..."
          else
            "Could not open the recent workspace..."
          UiHandler.root.modalDialogPool.pushDialog(
            new NotificationDialog(s"$errorMessage\n($exception)\nI will create a default one"))

          root.workspaceView.createDefaultWorkspace()
          Settings.get.recentWorkspace = None
        case Success(_) =>
      }
    }

    new Thread(() => {
      while (true) {
        Profiler.startTimeMeasurement("tick")

        withTickLockAcquired(() => {
          workspace.update()
          tpsCounter.tick()
        })

        Profiler.endTimeMeasurement("tick")
        ticker.waitNext()
      }
    }).start()
    splashScreen.dispose()
    logger.info("Ocelot Desktop is up and ready!")
    UiHandler.start()

    logger.info("Cleaning up")
    Settings.save(settingsFile)
    ResourceManager.freeResources()
    UiHandler.terminate()

    Ocelot.shutdown()

    logger.info("Thanks for using Ocelot Desktop")
    System.exit(0)
  }

  def main(rawArgs: Array[String]): Unit = {
    val args = CommandLine.parse(rawArgs)
    if (args.contains(CommandLine.Help)) {
      println(CommandLine.doc)
    } else {
      try mainInner(args)
      catch {
        case e: Exception =>
          val sw = new StringWriter
          val pw = new PrintWriter(sw)
          e.printStackTrace(pw)
          logger.error(s"${sw.toString}")

          System.exit(1)
      }
    }
  }

  private def saveWorld(nbt: NBTTagCompound): Unit = withTickLockAcquired(() => {
    val backendNBT = new NBTTagCompound
    val frontendNBT = new NBTTagCompound
    workspace.save(backendNBT)
    root.workspaceView.save(frontendNBT)
    frontendNBT.setNewTagList("players", players.map(player => new NBTTagString(player.nickname)))
    nbt.setTag("back", backendNBT)
    nbt.setTag("front", frontendNBT)
  })

  private def loadWorld(nbt: NBTTagCompound): Unit = withTickLockAcquired(() => {
    val backendNBT = nbt.getCompoundTag("back")
    val frontendNBT = nbt.getCompoundTag("front")
    workspace.load(backendNBT)
    root.workspaceView.load(frontendNBT)
    if (frontendNBT.hasKey("players")) {
      players.clear()
      players.addAll(frontendNBT.getTagList("players", NBT.TAG_STRING).map((player: NBTTagString) => User(player.getString)))
    }
  })

  private var savePath: Option[Path] = None
  private val tmpPath = Files.createTempDirectory("ocelot-save")

  def newWorkspace(): Unit = {
    root.workspaceView.newWorkspace()
    Settings.get.recentWorkspace = None
  }

  def save(): Unit = {
    if (savePath.isEmpty) {
      saveAs()
      return
    }

    val oldPath = workspace.path
    val newPath = savePath.get
    if (oldPath != newPath) {
      val oldFiles = Files.list(oldPath).iterator.asScala.toArray
      val newFiles = Files.list(newPath).iterator.asScala.toArray
      val toRemove = newFiles.intersect(oldFiles)

      for (path <- toRemove) {
        if (Files.isDirectory(path)) {
          FileUtils.deleteDirectory(path.toFile)
        } else {
          Files.delete(path)
        }
      }

      for (path <- oldFiles) {
        val oldFile = oldPath.resolve(path.getFileName).toFile
        val newFile = newPath.resolve(path.getFileName).toFile
        if (Files.isDirectory(path)) {
          FileUtils.copyDirectory(oldFile, newFile)
        } else {
          FileUtils.copyFile(oldFile, newFile)
        }
      }

      workspace.path = newPath
    }

    val path = newPath + "/workspace.nbt"
    val writer = new DataOutputStream(new FileOutputStream(path))
    val nbt = new NBTTagCompound
    saveWorld(nbt)
    CompressedStreamTools.writeCompressed(nbt, writer)
    writer.flush()
    logger.info(s"Saved workspace to: $newPath")
  }

  def saveAs(): Unit = {
    showFileChooserDialog(
      dir => {
        if (dir.isEmpty)
          return

        savePath = dir.map(_.toPath)
        Settings.get.recentWorkspace = dir.map(_.getCanonicalPath)
        save()
      },
      JFileChooser.SAVE_DIALOG,
      JFileChooser.DIRECTORIES_ONLY
    )
  }

  def open(): Unit = {
    showFileChooserDialog(load, JFileChooser.OPEN_DIALOG, JFileChooser.DIRECTORIES_ONLY)
  }

  def load(dir: Option[File]): Try[Unit] = {
    if (dir.isDefined) {
      val path = Paths.get(dir.get.getCanonicalPath, "workspace.nbt")
      if (Files.exists(path)) {
        Try {
          val reader = new DataInputStream(Files.newInputStream(path))
          val nbt = CompressedStreamTools.readCompressed(reader)
          savePath = Some(dir.get.toPath)
          Settings.get.recentWorkspace = dir.map(_.getCanonicalPath)
          workspace.path = dir.get.toPath
          loadWorld(nbt)
        }
      } else Failure(new FileNotFoundException("Specified directory does not contain 'workspace.nbt'"))
    } else Failure(new IllegalArgumentException("Received None as a directory path"))
  }

  def showFileChooserDialog(fun: Option[File] => Unit, dialogType: Int, selectionMode: Int): Unit = {
    new Thread(() => {
      val lastFile = savePath.map(_.toFile).orNull
      val chooser: JFileChooser = try {
        new NativeJFileChooser(lastFile)
      } catch {
        case _: Throwable => new JFileChooser(lastFile)
      }

      chooser.setFileSelectionMode(selectionMode)
      chooser.setDialogType(dialogType)

      val option = chooser.showDialog(null, null)
      fun(if (option == JFileChooser.APPROVE_OPTION) Some(chooser.getSelectedFile) else None)
    }).start()
  }

  def addPlayerDialog(): Unit = {
    UiHandler.root.modalDialogPool.pushDialog(new AddPlayerDialog())
  }

  def player: User = if (players.nonEmpty) players.head else User("myself")

  def selectPlayer(name: String): Unit = {
    players.indexWhere(_.nickname == name) match {
      case -1 => players.prepend(User(name))
      case i =>
        val player = players(i)
        players.remove(i)
        players.prepend(player)
    }
  }

  def removePlayer(name: String): Unit = {
    players.indexWhere(_.nickname == name) match {
      case -1 =>
      case i => players.remove(i)
    }
  }

  def settings(): Unit = {
    UiHandler.root.modalDialogPool.pushDialog(new SettingsDialog())
  }

  def cleanup(): Unit = {
    FileUtils.deleteDirectory(tmpPath.toFile)
  }

  def exit(): Unit = {
    if (UiHandler.root.modalDialogPool.children.exists(_.isInstanceOf[ExitConfirmationDialog]))
      return

    if (savePath.isDefined) {
      save()
      UiHandler.exit()
      cleanup()
      return
    }

    UiHandler.root.modalDialogPool.pushDialog(new ExitConfirmationDialog {
      override def onSaveSelected(): Unit = {
        showFileChooserDialog(
          dir => {
            if (dir.isEmpty)
              return

            savePath = dir.map(_.toPath)
            Settings.get.recentWorkspace = dir.map(_.getCanonicalPath)
            save()
            UiHandler.exit()
            cleanup()
          },
          JFileChooser.SAVE_DIALOG,
          JFileChooser.DIRECTORIES_ONLY
        )
      }

      override def onExitSelected(): Unit = {
        UiHandler.exit()
        cleanup()
      }
    })
  }

  var workspace: Workspace = _

  private def createWorkspace(): Unit = {
    workspace = new Workspace(tmpPath)
  }

  private def setupEventHandlers(): Unit = {
    EventBus.listenTo(classOf[BeepEvent], { case event: BeepEvent =>
      if (!UiHandler.audioDisabled)
        Audio.beep(event.frequency, event.duration)
      else
        logger.info(s"Beep ${event.frequency} Hz for ${event.duration} ms")
    })

    EventBus.listenTo(classOf[BeepPatternEvent], { case event: BeepPatternEvent =>
      if (!UiHandler.audioDisabled)
        Audio.beep(event.pattern)
      else
        logger.info(s"Beep pattern `${event.pattern}``")
    })

    EventBus.listenTo(classOf[MachineCrashEvent], { case event: MachineCrashEvent =>
      logger.info(s"[EVENT] Machine crash! (address = ${event.address}, ${event.message})")
    })

    if (!UiHandler.audioDisabled) {
      val soundFloppyAccess = Audio.FloppyAccess.map(buffer => new SoundSource(buffer))
      val soundHDDAccess = Audio.HDDAccess.map(buffer => new SoundSource(buffer))
      EventBus.listenTo(classOf[FileSystemActivityEvent], { case event: FileSystemActivityEvent =>
        val sound = if (event.activityType == Floppy) soundFloppyAccess else soundHDDAccess
        sound(random.nextInt(sound.length)).play()
      })
    }
  }
}
