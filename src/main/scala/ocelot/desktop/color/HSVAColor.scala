package ocelot.desktop.color

case class HSVAColor(h: Float, s: Float, v: Float, a: Float) extends Color {
  assert(0 <= h && h <= 360, "Invalid HUE channel")
  assert(0 <= s && s <= 1, "Invalid SATURATION channel")
  assert(0 <= v && v <= 1, "Invalid VALUE channel")
  assert(0 <= a && a <= 1, "Invalid ALPHA channel")

  override def toInt: IntColor = toRGBA.toInt

  override def toRGBA: RGBAColor = toRGBANorm.toRGBA

  override def toRGBANorm: RGBAColorNorm = {
    def f(n: Float): Float = {
      val k = (n + h / 60f) % 6f
      v - v * s * k.min(4f - k).min(1f).max(0f)
    }

    RGBAColorNorm(f(5), f(3), f(1), a)
  }

  override def toHSVA: HSVAColor = this
}
