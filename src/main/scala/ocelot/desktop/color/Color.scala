package ocelot.desktop.color

object Color {
  val White: RGBAColorNorm = RGBAColorNorm(1, 1, 1)
  val Black: RGBAColorNorm = RGBAColorNorm(0, 0, 0)
  val Yellow: RGBAColorNorm = RGBAColorNorm(1.0f, 1.0f, 0.33f)
  val Cyan: RGBAColorNorm = RGBAColorNorm(0.33f, 1.0f, 1.0f)
  val Grey: RGBAColorNorm = RGBAColorNorm(0.5f, 0.5f, 0.5f)
  val Transparent: RGBAColorNorm = RGBAColorNorm(0, 0, 0, 0)
}

trait Color {
  def toInt: IntColor

  def toRGBA: RGBAColor

  def toRGBANorm: RGBAColorNorm

  def toHSVA: HSVAColor
}
