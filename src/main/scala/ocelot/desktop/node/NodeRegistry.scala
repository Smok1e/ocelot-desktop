package ocelot.desktop.node

import ocelot.desktop.node.nodes._
import totoro.ocelot.brain.entity.{Cable, Case, FloppyDiskDrive, Relay, Screen}

import scala.collection.mutable

object NodeRegistry {
  val types: mutable.ArrayBuffer[NodeType] = mutable.ArrayBuffer[NodeType]()

  def register(t: NodeType): Unit = {
    types += t
  }

  for (tier <- 0 to 2) {
    register(NodeType("Screen" + tier, "nodes/Screen", tier, () => {
      new ScreenNode(new Screen(tier))
    }))
  }

  register(NodeType("Disk Drive", "nodes/DiskDrive", -1, () => {
    new DiskDriveNode(new FloppyDiskDrive())
  }))

  for (tier <- 0 to 3) {
    register(NodeType("Computer" + tier, "nodes/Computer", tier, () => {
      new ComputerNode(new Case(tier))
    }))
  }

  register(NodeType("Relay", "nodes/Relay", -1, () => {
    new RelayNode(new Relay)
  }))

  register(NodeType("Cable", "nodes/Cable", -1, () => {
    new CableNode(new Cable)
  }))
}
