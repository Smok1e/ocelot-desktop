package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.IntColor
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.slot.FloppySlot
import totoro.ocelot.brain.entity.FloppyDiskDrive
import totoro.ocelot.brain.entity.traits.{Environment, Floppy}
import totoro.ocelot.brain.loot.Loot
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.DyeColor

class DiskDriveNode(val diskDrive: FloppyDiskDrive) extends Node {
  var lastAccess = 0L

  OcelotDesktop.workspace.add(diskDrive)

  val slot: FloppySlot = new FloppySlot(diskDrive.inventory(0))
  slot.item = floppy.getOrElse(Loot.OpenOsFloppy.create())

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[FloppyDiskDrive]
    })
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    nbt.setString("address", diskDrive.node.address)
  }

  override def environment: Environment = diskDrive

  override def icon: String = "nodes/DiskDrive"

  override protected val canOpen = true

  val colorMap = Map(
    DyeColor.BLACK -> 0x444444, // 0x1E1B1B
    DyeColor.RED -> 0xB3312C,
    DyeColor.GREEN -> 0x339911, // 0x3B511A
    DyeColor.BROWN -> 0x51301A,
    DyeColor.BLUE -> 0x6666FF, // 0x253192
    DyeColor.PURPLE -> 0x7B2FBE,
    DyeColor.CYAN -> 0x66FFFF, // 0x287697
    DyeColor.SILVER -> 0xABABAB,
    DyeColor.GRAY -> 0x666666, // 0x434343
    DyeColor.PINK -> 0xD88198,
    DyeColor.LIME -> 0x66FF66, // 0x41CD34
    DyeColor.YELLOW -> 0xFFFF66, // 0xDECF2A
    DyeColor.LIGHT_BLUE -> 0xAAAAFF, // 0x6689D3
    DyeColor.MAGENTA -> 0xC354CD,
    DyeColor.ORANGE -> 0xEB8844,
    DyeColor.WHITE -> 0xF0F0F0
  )

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    if (System.currentTimeMillis() - diskDrive.lastDiskAccess < 400 && Math.random() > 0.1)
      g.sprite("nodes/DiskDriveActivity", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (slot.item.isDefined)
      g.sprite("nodes/DiskDriveFloppy", position.x + 2, position.y + 2, size.width - 4, size.height - 4, IntColor(colorMap(slot.item.get.color)))
  }

  override val window: Option[DiskDriveWindow] = Some(new DiskDriveWindow(this))

  private def floppy: Option[Floppy] = {
    diskDrive.inventory(0).get match {
      case Some(floppy: Floppy) => Some(floppy)
      case _ => None
    }
  }
}