package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.node.Node
import totoro.ocelot.brain.entity.Cable
import totoro.ocelot.brain.entity.traits.Environment
import totoro.ocelot.brain.nbt.NBTTagCompound

class CableNode(cable: Cable) extends Node {
  OcelotDesktop.workspace.add(cable)

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      if (address == "")
        new Cable
      else
        OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[Cable]
    })
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    if (cable.node.address != null)
      nbt.setString("address", cable.node.address)
  }

  override def environment: Environment = cable

  override def icon: String = "nodes/Cable"

  override def minimumSize: Size2D = Size2D(36, 36)

  override protected val exposeAddress = false
}
