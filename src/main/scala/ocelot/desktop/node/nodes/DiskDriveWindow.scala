package ocelot.desktop.node.nodes

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.{AlignItems, Layout, LinearLayout}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.ui.widget.{Label, PaddingBox, Widget}
import ocelot.desktop.util.{DrawUtils, Orientation}

class DiskDriveWindow(diskDrive: DiskDriveNode) extends BasicWindow {
  private val inner = new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical, alignItems = AlignItems.Center)

    children :+= new PaddingBox(new Label {
      override def text: String = diskDrive.labelOrAddress.take(8)
      override def isSmall: Boolean = true
      override def color: Color = ColorScheme("ComputerAddress")
    }, Padding2D(bottom = 8))

    children :+= new PaddingBox(diskDrive.slot, Padding2D(top = 8, bottom = 8))
  }

  children :+= new PaddingBox(inner, Padding2D(10, 12, 10, 12))

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
