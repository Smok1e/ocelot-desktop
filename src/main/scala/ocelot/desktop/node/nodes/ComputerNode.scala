package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.audio.{Audio, SoundSource}
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.event.sources.KeyEvents
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.slot._
import ocelot.desktop.util.{ResourceManager, TierColor}
import org.lwjgl.input.Keyboard
import totoro.ocelot.brain.entity.traits.{Computer, Entity, Floppy, GenericCPU, Inventory}
import totoro.ocelot.brain.entity.{CPU, Case, EEPROM, GraphicsCard, HDDManaged, HDDUnmanaged, Memory}
import totoro.ocelot.brain.loot.Loot
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.Tier

import scala.reflect.ClassTag

class ComputerNode(val computer: Case, setup: Boolean = true) extends Node {
  var lastFilesystemAccess = 0L

  var eepromSlot: EEPROMSlot = _
  var cpuSlot: CPUSlot = _
  var memorySlots: Array[MemorySlot] = _
  var cardSlots: Array[CardSlot] = _
  var diskSlots: Array[DiskSlot] = _
  var floppySlot: Option[FloppySlot] = None

  val soundComputerRunning = new SoundSource(Audio.ComputerRunning, true)

  setupSlots()

  if (setup) {
    cpuSlot.owner.put(new CPU(computer.tier.min(2)))
    memorySlots(0).owner.put(new Memory(computer.tier.min(2) * 2 + 1))
    memorySlots(1).owner.put(new Memory(computer.tier.min(2) * 2 + 1))
    cardSlots(0).owner.put(new GraphicsCard(computer.tier.min(1)))
    floppySlot.map(_.owner).foreach(_.put(Loot.OpenOsFloppy.create()))
    eepromSlot.owner.put(Loot.OpenOsEEPROM.create())
//    refitSlots()
    OcelotDesktop.workspace.add(computer)
  }

  refitSlots()

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[Case]
    }, setup = false)
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    nbt.setString("address", computer.node.address)
  }

  override def environment: Computer = computer

  override val icon: String = "nodes/Computer"
  override def iconColor: Color = TierColor.get(computer.tier)

  override protected val canOpen = true

  def turnOn(): Unit = {
    computer.turnOn()
    soundComputerRunning.play()
  }

  def turnOff(): Unit = {
    computer.turnOff()
    soundComputerRunning.stop()
  }

  def isRunning: Boolean = computer.machine.isRunning

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (isRunning) {
      menu.addEntry(new ContextMenuEntry("Turn off", () => turnOff()))
      menu.addEntry(new ContextMenuEntry("Reboot", () => {
        computer.turnOff()
        computer.turnOn()
      }))
    } else
      menu.addEntry(new ContextMenuEntry("Turn on", () => turnOn()))

    menu.addEntry(new ContextMenuSubmenu("Set tier") {
      addEntry(new ContextMenuEntry("Tier 1", () => changeTier(Tier.One)))
      addEntry(new ContextMenuEntry("Tier 2", () => changeTier(Tier.Two)))
      addEntry(new ContextMenuEntry("Tier 3", () => changeTier(Tier.Three)))
      addEntry(new ContextMenuEntry("Creative", () => changeTier(Tier.Four)))
    })

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  override def onClick(event: ClickEvent): Unit = {
    event match {
      case ClickEvent(MouseEvent.Button.Left, _) =>
        if (KeyEvents.isDown(Keyboard.KEY_LSHIFT))
          if (isRunning)
            turnOff()
          else
            turnOn()
        else
          super.onClick(event)
      case event => super.onClick(event)
    }
  }

  private def changeTier(n: Int): Unit = {
    computer.tier = n
    setupSlots()
    refitSlots()
    if (currentWindow != null) currentWindow.updateSlots()
  }

  private def slotAccepts(slot: Inventory#Slot, entity: Entity): Boolean = entity match {
    case cpu: GenericCPU => cpuSlot.owner.index == slot.index && cpuSlot.tier >= cpu.cpuTier
    case mem: Memory => memorySlots
      .exists(memSlot => memSlot.owner.index == slot.index && memSlot.tier >= (mem.tier + 1) / 2 - 1)
    case hdd: HDDManaged => diskSlots
      .exists(diskSlot => diskSlot.owner.index == slot.index && diskSlot.tier >= hdd.tier)
    case hdd: HDDUnmanaged => diskSlots
      .exists(diskSlot => diskSlot.owner.index == slot.index && diskSlot.tier >= hdd.tier)
    case _: EEPROM => eepromSlot.owner.index == slot.index
    case _: Floppy => floppySlot.exists(_.owner.index == slot.index)
    case card: Entity => cardSlots
      .exists(cardSlot => cardSlot.owner.index == slot.index && cardSlot.tier >= CardRegistry.getTier(card))
  }

  private def isSlotValid(slot: Inventory#Slot): Boolean = slot.get.exists(slotAccepts(slot, _))

  private def reloadSlots(): Unit = {
    cpuSlot.reloadItem()
    memorySlots.foreach(_.reloadItem())
    cardSlots.foreach(_.reloadItem())
    diskSlots.foreach(_.reloadItem())
    eepromSlot.reloadItem()
    floppySlot.foreach(_.reloadItem())
  }

  private def refitSlots(): Unit = {
    if (computer.inventory.forall(isSlotValid)) {
      reloadSlots()

      return
    }

    val entities = computer.inventory.entities.toArray
    computer.inventory.clear()

    cpuSlot._item = None
    for (slot <- memorySlots) slot._item = None
    for (slot <- cardSlots) slot._item = None
    for (slot <- diskSlots) slot._item = None
    eepromSlot._item = None
    floppySlot.foreach(_._item = None)

    def findBestSlot[T <: InventorySlot[_]](entity: Entity, candidates: Array[T], tierProvider: T => Option[Int]): Option[T] = {
      candidates.iterator
        .filter(_.owner.isEmpty)
        .filter(slot => slotAccepts(slot.owner, entity))
        .minByOption(tierProvider(_).getOrElse(Int.MinValue))
    }

    for (entity <- entities) {
      val newSlot = entity match {
        case _: GenericCPU => findBestSlot[CPUSlot](entity, Array(cpuSlot), slot => Some(slot.tier))
        case _: Memory => findBestSlot[MemorySlot](entity, memorySlots, slot => Some(slot.tier))
        case _: HDDManaged => findBestSlot[DiskSlot](entity, diskSlots, slot => Some(slot.tier))
        case _: HDDUnmanaged => findBestSlot[DiskSlot](entity, diskSlots, slot => Some(slot.tier))
        case _: EEPROM => findBestSlot[EEPROMSlot](entity, Array(eepromSlot), _ => None)
        case _: Floppy => findBestSlot[FloppySlot](entity, floppySlot.toArray, _ => None)
        case _: Entity => findBestSlot[CardSlot](entity, cardSlots, slot => Some(slot.tier))
      }

      newSlot.foreach(_.owner.put(entity))
    }

    reloadSlots()
  }

  private def setupSlots(): Unit = {
    var slotIndex = 0

    def nextSlot(): computer.Slot = {
      val result = computer.inventory(slotIndex)
      slotIndex += 1
      result
    }

    def addSlot[T <: InventorySlot[_]](factory: computer.Slot => T): T = {
      val slot = nextSlot()
      val widget = factory(slot)

      widget
    }

    def addSlots[T <: InventorySlot[_] : ClassTag](factories: (computer.Slot => T)*): Array[T] = {
      val array = Array.newBuilder[T]

      for (factory <- factories) {
        array += addSlot(factory)
      }

      array.result()
    }

    computer.tier match {
      case Tier.One =>
        cardSlots = addSlots(new CardSlot(_, Tier.One), new CardSlot(_, Tier.One))
        memorySlots = addSlots(new MemorySlot(_, Tier.One))
        diskSlots = addSlots(new DiskSlot(_, Tier.One))
        floppySlot = None
        cpuSlot = addSlot(new CPUSlot(_, this, Tier.One))
        // no idea why on earth the memory slots are split in two here
        memorySlots :+= addSlot(new MemorySlot(_, Tier.One))
        eepromSlot = addSlot(new EEPROMSlot(_))

      case Tier.Two =>
        cardSlots = addSlots(new CardSlot(_, Tier.Two), new CardSlot(_, Tier.One))
        memorySlots = addSlots(new MemorySlot(_, Tier.Two), new MemorySlot(_, Tier.Two))
        diskSlots = addSlots(new DiskSlot(_, Tier.Two), new DiskSlot(_, Tier.One))
        floppySlot = None
        cpuSlot = addSlot(new CPUSlot(_, this, Tier.Two))
        eepromSlot = addSlot(new EEPROMSlot(_))

      case _ =>
        cardSlots = if (computer.tier == Tier.Three) {
          addSlots(new CardSlot(_, Tier.Three), new CardSlot(_, Tier.Two), new CardSlot(_, Tier.Two))
        } else {
          addSlots(new CardSlot(_, Tier.Three), new CardSlot(_, Tier.Three), new CardSlot(_, Tier.Three))
        }

        memorySlots = addSlots(new MemorySlot(_, Tier.Three), new MemorySlot(_, Tier.Three))

        diskSlots = if (computer.tier == Tier.Three) {
          addSlots(new DiskSlot(_, Tier.Three), new DiskSlot(_, Tier.Two))
        } else {
          addSlots(new DiskSlot(_, Tier.Three), new DiskSlot(_, Tier.Three))
        }

        floppySlot = Some(addSlot(new FloppySlot(_)))
        cpuSlot = addSlot(new CPUSlot(_, this, Tier.Three))
        eepromSlot = addSlot(new EEPROMSlot(_))
    }
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    val hasErrored = computer.machine.lastError != null

    if (isRunning && !hasErrored)
      g.sprite("nodes/ComputerOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (!isRunning && hasErrored)
      g.sprite("nodes/ComputerErrorOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)

    if (isRunning && System.currentTimeMillis() - computer.machine.lastDiskAccess < 400 && Math.random() > 0.1)
      g.sprite("nodes/ComputerActivityOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  override def update(): Unit = {
    super.update()
    if (!isRunning && soundComputerRunning.isPlaying)
      soundComputerRunning.stop()
    if (isHovered || isMoving)
      root.get.statusBar.addKeyMouseEntry("icons/LMB", "SHIFT", if (isRunning) "Turn Off" else "Turn On")
  }

  private var currentWindow: ComputerWindow = _

  override def window: Option[ComputerWindow] = {
    if (currentWindow == null) {
      currentWindow = new ComputerWindow(this)
      Some(currentWindow)
    } else Some(currentWindow)
  }

  override def dispose(): Unit = {
    ResourceManager.freeResource(soundComputerRunning)
    super.dispose()
  }
}
