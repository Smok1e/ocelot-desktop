package ocelot.desktop.node.nodes

import ocelot.desktop.color.{IntColor, RGBAColor}
import ocelot.desktop.geometry.{Rect2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.event.sources.{KeyEvents, MouseEvents}
import ocelot.desktop.ui.event.{DragEvent, KeyEvent, MouseEvent, ScrollEvent}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.{DrawUtils, Logging}
import ocelot.desktop.{ColorScheme, OcelotDesktop}
import org.apache.commons.lang3.StringUtils
import org.lwjgl.input.Keyboard
import totoro.ocelot.brain.entity.Screen
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.util.{PackedColor, Tier}

class ScreenWindow(screenNode: ScreenNode) extends BasicWindow with Logging {
  private val fontWidth = 8f
  private val fontHeight = 16f

  private var lastMousePos = Vector2D(0, 0)
  private var sentTouchEvent = false
  private var startingWidth = 0f
  private var scaleDragPoint: Option[Vector2D] = None

  private var _scale = 1f
  private var scaleX: Float = 1f
  private var scaleY: Float = 1f

  private def scale: Float = _scale

  private def scale_=(value: Float): Any = {
    _scale = value

    scaleX = (fontWidth * scale).floor / fontWidth
    scaleY = (fontHeight * scale).floor / fontHeight
  }

  private def screen: Screen = screenNode.screen

  private def screenWidth: Int = screen.getWidth

  private def screenHeight: Int = screen.getHeight

  override def minimumSize: Size2D = Size2D(screenWidth * fontWidth * scaleX + 32, screenHeight * scaleY * fontHeight + 36)

  override def receiveScrollEvents: Boolean = true

  eventHandlers += {
    case event: KeyEvent if shouldHandleKeys =>
      event.state match {
        case KeyEvent.State.Press | KeyEvent.State.Repeat =>
          screen.keyDown(event.char, event.code, OcelotDesktop.player)

          // note: in OpenComputers, key_down signal is fired __before__ clipboard signal
          if (event.code == Keyboard.KEY_INSERT)
            screen.clipboard(UiHandler.clipboard, OcelotDesktop.player)

        case KeyEvent.State.Release =>
          screen.keyUp(event.char, event.code, OcelotDesktop.player)
      }

    case event: MouseEvent if shouldHandleKeys =>
      val pos = convertMousePos(UiHandler.mousePosition)
      val inside = checkBounds(pos)

      if (inside)
        lastMousePos = pos

      event.state match {
        case MouseEvent.State.Press if inside && screenNode.screen.tier > Tier.One =>
          screen.mouseDown(pos.x, pos.y, event.button.id, OcelotDesktop.player)
          sentTouchEvent = true

        case MouseEvent.State.Release =>
          if (event.button == MouseEvent.Button.Middle && inside)
            screen.clipboard(UiHandler.clipboard, OcelotDesktop.player)

          if (sentTouchEvent) {
            screen.mouseUp(lastMousePos.x, lastMousePos.y, event.button.id, OcelotDesktop.player)
            sentTouchEvent = false
          } else if (closeButtonBounds.contains(UiHandler.mousePosition)) {
            hide()
          }

        case _ =>
      }

    case event: ScrollEvent if shouldHandleKeys && screenNode.screen.tier > Tier.One =>
      screen.mouseScroll(lastMousePos.x, lastMousePos.y, event.offset, OcelotDesktop.player)

    case ev@DragEvent(DragEvent.State.Start, MouseEvent.Button.Left, _) =>
      if (scaleDragRegion.contains(ev.start)) {
        scaleDragPoint = Some(ev.start)
        startingWidth = screenWidth * fontWidth * scaleX
      }

    case DragEvent(DragEvent.State.Drag, MouseEvent.Button.Left, mousePos) =>
      for (point <- scaleDragPoint) {
        val sx = point.x - mousePos.x
        val sy = point.y - mousePos.y

        if (sx.abs > sy.abs) {
          val newWidth = startingWidth - sx
          val maxWidth = screenWidth * fontWidth
          var midScale = (newWidth / maxWidth).max(0f)

          if (!KeyEvents.isDown(Keyboard.KEY_LSHIFT) && scale <= 1.001)
            midScale = midScale.min(1f)

          var lowScale = (fontWidth * midScale).floor / fontWidth
          val highScale = (fontWidth * midScale).ceil / fontWidth

          // enforce minimal screen size
          if (lowScale < 0.3f) {
            lowScale = 0.25f
          }

          scale = if (midScale - lowScale > highScale - midScale) highScale else lowScale
        }
        else {
          val newHeight = startingWidth * (screenHeight * fontHeight / screenWidth / fontWidth) - sy
          val maxHeight = screenHeight * fontHeight
          var midScale = (newHeight / maxHeight).max(0f)

          if (!KeyEvents.isDown(Keyboard.KEY_LSHIFT) && scale <= 1.001)
            midScale = midScale.min(1f)

          val lowScale = (fontHeight * midScale).floor / fontHeight
          val highScale = (fontHeight * midScale).ceil / fontHeight
          scale = if (midScale - lowScale > highScale - midScale) highScale else lowScale
        }
      }

    case DragEvent(DragEvent.State.Stop, MouseEvent.Button.Left, _) =>
      scaleDragPoint = None
  }

  private def shouldHandleKeys: Boolean = isFocused && !root.get.modalDialogPool.isVisible

  override def save(nbt: NBTTagCompound): Unit = {
    nbt.setFloat("scale", scale)
    super.save(nbt)
  }

  override def load(nbt: NBTTagCompound): Unit = {
    scale = nbt.getFloat("scale")
    super.load(nbt)
  }

  override def show(): Unit = {
    scale = math.min(
      ((UiHandler.root.width - 16) / (screenWidth * fontWidth + 32)).min(1f).max(0f),
      ((UiHandler.root.height - 32) / (screenHeight * fontHeight + 36)).min(1f).max(0f)
    )

    super.show()
  }

  private def checkBounds(p: Vector2D): Boolean = p.x >= 0 && p.y >= 0 && p.x < screenWidth && p.y < screenHeight

  private def convertMousePos(p: Vector2D): Vector2D = {
    if (screenNode.screen.getPrecisionMode) {
      Vector2D(
        (p.x - 16f - position.x) / fontWidth / scaleX,
        (p.y - 16f - position.y) / fontHeight / scaleY
      )
    } else {
      Vector2D(
        math.floor((p.x - 16f - position.x) / fontWidth / scaleX),
        math.floor((p.y - 16f - position.y) / fontHeight / scaleY)
      )
    }
  }

  override protected def dragRegions: Iterator[Rect2D] = Iterator(
    Rect2D(position.x, position.y, size.width, 20),
    Rect2D(position.x, position.y + size.height - 16, size.width - 16, 16),
    Rect2D(position.x, position.y, 16, size.height),
    Rect2D(position.x + size.width - 16, position.y, 16, size.height - 16),
  )

  private def scaleDragRegion: Rect2D = Rect2D(position.x + size.width - 16, position.y + size.height - 16, 16, 16)

  override def update(): Unit = {
    super.update()

    if (scaleDragPoint.isDefined || scaleDragRegion.contains(UiHandler.mousePosition)) {
      root.get.statusBar.addMouseEntry("icons/DragLMB", "Scale screen")
      root.get.statusBar.addKeyMouseEntry("icons/DragLMB", "SHIFT", "Scale screen (magnify)")
    }

    val currentMousePos = convertMousePos(UiHandler.mousePosition)
    if (!checkBounds(currentMousePos) || currentMousePos == lastMousePos) return

    lastMousePos = currentMousePos

    if (isFocused && screenNode.screen.tier > Tier.One)
      for (button <- MouseEvents.pressedButtons) {
        screen.mouseDrag(lastMousePos.x, lastMousePos.y, button.id, OcelotDesktop.player)
      }
  }

  private def closeButtonBounds: Rect2D = Rect2D(
    position.x + screenWidth * fontWidth * scaleX + 2,
    position.y - 2, 22, 22)

  override def draw(g: Graphics): Unit = {
    beginDraw(g)

    val startX = position.x + 16
    val startY = position.y + 20
    val windowWidth = screenWidth * fontWidth * scaleX
    val windowHeight = screenHeight * fontHeight * scaleY

    DrawUtils.shadow(g, startX - 22, startY - 22, windowWidth + 44, windowHeight + 52, 0.5f)
    DrawUtils.screenBorder(g, startX, startY, windowWidth, windowHeight)

    var color: Short = 0

    if (screen.getPowerState) {
      for (y <- 0 until screenHeight) {
        for (x <- 0 until screenWidth) {
          if (x == 0 || g.font.charWidth(screen.data.buffer(y)(x - 1)) != 16) {
            color = screen.data.color(y)(x)

            g.background = IntColor(PackedColor.unpackBackground(color, screen.data.format))
            g.foreground = IntColor(PackedColor.unpackForeground(color, screen.data.format))

            g.char(
              startX + x * fontWidth * scaleX,
              startY + y * fontHeight * scaleY,
              screen.data.buffer(y)(x),
              scaleX,
              scaleY
            )
          }
        }
      }
    }
    else {
      g.rect(startX, startY, windowWidth, windowHeight, ColorScheme("ScreenOff"))
    }

    g.setSmallFont()
    g.background = RGBAColor(0, 0, 0, 0)
    g.foreground = RGBAColor(110, 110, 110)

    val freeSpace = ((windowWidth - 15) / 8).toInt
    val label = screenNode.labelOrAddress
    val text = if (label.length <= freeSpace)
      label
    else
      StringUtils.substring(label, 0, (freeSpace - 1).max(0).min(label.length)) + "…"

    g.text(startX - 4, startY - 14, text)
    g.setNormalFont()

    g.sprite("window/CloseButton", startX + windowWidth - 7, startY - 13, 7, 6)

    endDraw(g)
  }
}
