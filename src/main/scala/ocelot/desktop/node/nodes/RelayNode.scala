package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.node.{Node, NodePort}
import totoro.ocelot.brain.entity.Relay
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.network
import totoro.ocelot.brain.util.Direction

class RelayNode(relay: Relay) extends Node {
  OcelotDesktop.workspace.add(relay)

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[Relay]
    })
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    nbt.setString("address", relay.sidedNode(Direction.South).address)
  }

  override def environment: Relay = relay

  override val icon: String = "nodes/Relay"

  override def ports: Array[NodePort] = Array(
    NodePort(Some(Direction.North)),
    NodePort(Some(Direction.South)),
    NodePort(Some(Direction.East)),
    NodePort(Some(Direction.West)),
    NodePort(Some(Direction.Up)),
    NodePort(Some(Direction.Down)))

  override def getNodeByPort(port: NodePort): network.Node = relay.sidedNode(port.direction.get)

  override protected val exposeAddress = false
}
