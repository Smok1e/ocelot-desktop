package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.util.TierColor
import totoro.ocelot.brain.entity.traits.Environment
import totoro.ocelot.brain.entity.{Keyboard, Screen}
import totoro.ocelot.brain.nbt.NBTTagCompound

class ScreenNode(var screen: Screen, setup: Boolean = true) extends Node {
  if (setup) {
    val keyboard = new Keyboard
    OcelotDesktop.workspace.add(screen)
    OcelotDesktop.workspace.add(keyboard)
    screen.connect(keyboard)
  }

  def this(nbt: NBTTagCompound) {
    this({
      val address = nbt.getString("address")
      OcelotDesktop.workspace.entityByAddress(address).get.asInstanceOf[Screen]
    }, setup = false)
    super.load(nbt)
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    nbt.setString("address", screen.node.address)
  }

  override def environment: Environment = screen

  override def icon: String = "nodes/Screen"

  override def iconColor: Color = TierColor.get(screen.tier)

  override protected val canOpen = true

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (screen.getPowerState)
      menu.addEntry(new ContextMenuEntry("Turn off", () => screen.setPowerState(false)))
    else
      menu.addEntry(new ContextMenuEntry("Turn on", () => screen.setPowerState(true)))

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    if (screen.getPowerState)
      g.sprite("nodes/ScreenOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  override lazy val window: Option[ScreenWindow] = Some(new ScreenWindow(this))
}
