package ocelot.desktop.node

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.util.Orientation

class SetLabelDialog(node: Node) extends ModalDialog {
  children :+= new PaddingBox(new Widget {
    override val layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text = "Set label"
    }, Padding2D(bottom = 16))

    children :+= new PaddingBox(new TextInput(node.label.getOrElse("")) {
      focus()
      override def onInput(text: String): Unit = {
        node.label = if (text.isEmpty) None else Some(text)
      }
      override def onConfirm(): Unit = close()
    }, Padding2D(bottom = 8))

    children :+= new Widget {
      children :+= new Filler
      children :+= new Button {
        override def text: String = "Close"
        override def onClick(): Unit = close()
      }
    }
  }, Padding2D.equal(16))
}
