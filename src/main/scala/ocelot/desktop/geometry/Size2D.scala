package ocelot.desktop.geometry

object Size2D {
  val Zero: Size2D = Size2D(0, 0)
  val Inf: Size2D = Size2D(Float.PositiveInfinity, Float.PositiveInfinity)

  def apply(vec: Vector2D): Size2D = Size2D(vec.x, vec.y)

  def apply(width: Double, height: Double): Size2D = Size2D(width.asInstanceOf[Float], height.asInstanceOf[Float])

  def apply(width: Int, height: Int): Size2D = Size2D(width.asInstanceOf[Float], height.asInstanceOf[Float])
}

case class Size2D(width: Float, height: Float) {
  def max(other: Size2D): Size2D = Size2D(width.max(other.width), height.max(other.height))

  def min(other: Size2D): Size2D = Size2D(width.min(other.width), height.min(other.height))

  def toVector: Vector2D = Vector2D(width, height)

  def clamped(min: Size2D, max: Size2D): Size2D = {
    Size2D(
      math.min(max.width, math.max(min.width, width)),
      math.min(max.height, math.max(min.height, height))
    )
  }

  def +(that: Size2D): Size2D = Size2D(width + that.width, height + that.height)

  def -(that: Size2D): Size2D = Size2D(width - that.width, height - that.height)

  def *(that: Size2D): Size2D = Size2D(width * that.width, height * that.height)

  def /(that: Size2D): Size2D = Size2D(width / that.width, height / that.height)

  def *(mul: Float): Size2D = Size2D(width * mul, height * mul)

  def /(mul: Float): Size2D = Size2D(width / mul, height / mul)

  override def toString: String = f"Size2D [ $width%8.2f x $height%8.2f ]"
}