PortDown  = #8382d8
PortUp    = #75bdc1
PortNorth = #c8ca5f
PortSouth = #990da3
PortEast  = #db7d75
PortWest  = #7ec95f
PortAny   = #9b9b9b

Tier0 = #ababab
Tier1 = #ffff66
Tier2 = #66ffff
Tier3 = #c354cd

Label = #333333

Scrollbar      = #e5e5e526
ScrollbarThumb = #cc3f72

ConnectionBorder = #191919
ConnectionNew    = #4c7f66
ConnectionDel    = #7f3333
Connection       = #909090

NodeSelectorRing       = #4c7f66
NodeSelectorBackground = #00000066

ContextMenuBackground = #222222cc
ContextMenuBorder     = #444444
ContextMenuText       = #b0b0b0
ContextMenuHover      = #333333
ContextMenuSeparator  = #444444

ComponentSelectorBackground = #222222cc
ComponentSelectorBorder     = #444444
ComponentSelectorText       = #b0b0b0

ComputerAddress = #333333

ScreenOff = #000000

StatusBar           = #181818
StatusBarBorder     = #222222
StatusBarForeground = #777777
StatusBarIcon       = #bbbbbb
StatusBarKey        = #bbbbbb
StatusBarTPS        = #77aa77
StatusBarFPS        = #7777aa

TitleBarBackground       = #181818
TitleBarBorder           = #222222
TitleBarButtonForeground = #bbbbbb
TitleBarButtonActive     = #333333

TextInputBackground    = #aaaaaa
TextInputBorder        = #888888
TextInputBorderFocused = #336666
TextInputForeground    = #333333
TextInputBorderError        = #aa8888
TextInputBorderErrorFocused = #cc6666

ButtonBackground = #aaaaaa
ButtonBorder     = #888888
ButtonForeground = #333333

BottomDrawerBorder = #888888

HistogramBarEmpty = #336633
HistogramBarTop   = #ccfdcc
HistogramBarFill  = #64cc65
HistogramGrid     = #336633
HistogramFill     = #73ff7360
HistogramEdge     = #ccfdcc

VerticalMenuBackground      = #c6c6c6
VerticalMenuEntryIcon       = #888888
VerticalMenuEntryActive     = #dfdfdf
VerticalMenuEntryForeground = #333333
VerticalMenuBorder          = #dfdfdf

SliderBackground = #aaaaaa
SliderBorder     = #888888
SliderHandler    = #bbbbbb
SliderForeground = #333333

CheckboxBackground = #aaaaaa
CheckboxBorder     = #888888
CheckboxForeground = #333333
CheckboxLabel      = #333333
