#version 120

varying vec4 fColor;
varying vec2 fUV;
varying vec4 fTextColor;
varying vec2 fTextUV;

uniform sampler2D uTexture;
uniform sampler2D uTextTexture;

void main() {
    float alpha = texture2D(uTextTexture, fTextUV).r;
    gl_FragColor = mix(texture2D(uTexture, fUV) * fColor, fTextColor, alpha);
}